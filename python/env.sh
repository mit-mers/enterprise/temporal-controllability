#!/bin/bash

diff .env/env_requirements.txt env_requirements.txt  >/dev/null 2>&1 || {
    echo "Environment changed. Updating environment."
    virtualenv-2.7 --no-site-packages --distribute .env
    source .env/bin/activate
    pip install -r env_requirements.txt 
    cp env_requirements.txt .env/env_requirements.txt
}
. .env/bin/activate
