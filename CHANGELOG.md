## v0.2.1
Date: April 15, 2022

### Added

+ Exposed delay controllability checker

## v0.2.0
Date: March 30, 2022

### Added

+ Float features

### removed

+ `mtk-utils` dependencies

## v0.1.1 - April 13, 2021

### Added

+ Check conflicts' existence in DC tests, and streamline their printing
  mechanism for manual inspection.

### Changed

+ Patch extension subpaths output by n3 implementation. (Discrepancies
  due to internal normalized representation of distance graph. n4
  implementation may still have bugs.)
+ Remove unnecessary regular edges for contingent constraints in
  distance graph.
+ Fix illegal symbol syntax in SC code.
+ Update CLPM dependencies.

## v0.1.0 - July 27, 2020

### Added

+ CLPM support
+ CI jobs for testing, excluding CCL
