;;; Copyright (c) 2013, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(defsystem #:temporal-controllability
  :name "Temporal Controllability"
  :version (:read-file-form "version.lisp-expr")
  :description "Contains algorithms for checking temporal controllability."
  :maintainer "MIT MERS Group"
  :components
  ((:file "package")
   (:module "src"
            :serial t
            :components
            ((:file "infinity")
             (:file "conditional-d-edge")
             (:file "dynamic-controllability")
             (:file "dc-incremental")
             (:file "dc-morris-n3")
             (:file "delay-controllability")
             (:file "ncycle")
             (:file "stnu-sc")
             (:file "temporal-conflict")
             (:file "controllability-checker"))))
  :depends-on (#:temporal-networks
               #:itc-v2
               #:queues.priority-queue
               #:s-xml
               #:alexandria
               #:bordeaux-threads
               #:float-features
               #:fiveam)
  :in-order-to ((test-op (load-op "temporal-controllability/test")))
  :perform (test-op (op c) (symbol-call :fiveam :run! :temporal-controllability)))

(defsystem #:temporal-controllability/test
  :version (:read-file-form "version.lisp-expr")
  :serial nil
  :components
  ((:module "tests"
            :serial t
            :components
            ((:file "package")
             (:file "test-sc")
             (:file "test-dc")
             (:file "test-delay"))))
  :depends-on (:fiveam :temporal-controllability))

(defmethod version-satisfies ((component (eql (find-system "temporal-controllability")))
                              version)
  (let ((actual-version (component-version component)))
    (and
     ;; The last known API breakage happened in v0.1.0.
     (version<= "0.1.0" version)
     (version<= version actual-version))))

#+:mtk-documentation
(eval-when (:compile-toplevel :load-toplevel :execute)
  (mtk-doc:register-doc-generator (:temporal-controllability)
    (:apiref :temporal-controllability)))
