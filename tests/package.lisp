(uiop:define-package #:temporal-controllability/test
                     (:use #:cl
                           #:fiveam
                           #:temporal-networks
                           #:temporal-controllability))

(in-package #:temporal-controllability/test)

(def-suite :temporal-controllability
           :description "Top-level test suite")

(def-suite :temporal-controllability/sc
           :in :temporal-controllability
           :description "Test dynamic controllability")

(def-suite :temporal-controllability/dc
           :in :temporal-controllability
           :description "Test dynamic controllability")

(def-suite :temporal-controllability/delay
           :in :temporal-controllability
           :description "Test delay controllability")
