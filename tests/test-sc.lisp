(in-package :temporal-controllability/test)
(in-suite :temporal-controllability/sc)

(defconstant +inf+ float-features:double-float-positive-infinity)

(defun verify-cycle (claim reference)
  (is-true (eql (length reference) (length claim)))
  (dolist (edge reference)
    (is-true (find edge claim :test #'equal))))

;;; The activity's duration is uncontrollable. It is followed by a
;;; controllable wait. There is an overall one hour time limit.
(test single-activity
  (let ((stnu (make-instance 'temporal-network
                             :features
                             '(:simple-temporal-constraints
                               :simple-contingent-temporal-constraints))))

    ;; The initial time windows for the activity and the wait make
    ;; the network just barely controllable.
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :from-event :e1 :to-event :e2
                   :lower-bound 20 :upper-bound 30
                   :id :activity)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e2 :to-event :e3
                   :lower-bound 30 :upper-bound 40
                   :id :wait)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 60
                   :id :time-limit)
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))

    ;; Widen the wait window, but shift it up so it cannot meet the
    ;; one hour requirement.
    (let ((wait-dur (find-temporal-constraint stnu :wait)))
      (setf (lower-bound wait-dur) 35)
      (setf (upper-bound wait-dur) 50))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-false controllable)
      (is-true neg-cycle)
      (verify-cycle neg-cycle
                    `((,(find-temporal-constraint stnu :activity) :ub)
                      (,(find-temporal-constraint stnu :wait) :lb)
                      (,(find-temporal-constraint stnu :time-limit) :ub))))

    ;; Now shift the wait window down to keep it within one hour, but
    ;; shorten the window so there is not enough controllability for
    ;; static scheduling.
    (let ((wait-dur (find-temporal-constraint stnu :wait)))
      (setf (lower-bound wait-dur) 25)
      (setf (upper-bound wait-dur) 30))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-false controllable)
      (is-true neg-cycle)
      (verify-cycle neg-cycle
                    `((,(find-temporal-constraint stnu :activity) :lb)
                      (,(find-temporal-constraint stnu :activity) :ub)
                      (,(find-temporal-constraint stnu :wait) :lb)
                      (,(find-temporal-constraint stnu :wait) :lb))))

    ;; Lower the activity's uncertain upper bound, so that it can be
    ;; controlled for within the five minute wait window.
    (let ((activity-dur (find-temporal-constraint stnu :activity)))
      (setf (upper-bound activity-dur) 25))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))))

;;; There are two activities in sequence. Both have controllable
;;; durations. There is an overall one hour time limit. This test
;;; should behave just as ITC would on an STN.
(test actually-an-stn
  (let ((stnu (make-instance 'temporal-network
                             :name :stnu-stn
                             :features
                             '(:simple-temporal-constraints
                               :simple-contingent-temporal-constraints))))

    ;; Both activities are capped at 30 minutes. This is controllable
    (make-instance 'simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 30
                   :id :activity-1)
    (make-instance 'simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 0 :upper-bound 30
                   :id :activity-2)
    (make-instance 'simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 60
                   :id :time-limit)
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))

    ;; The first activity's upper bound is increased. Because this is
    ;; an STN, it is still controllable.
    (let ((dur-1 (find-temporal-constraint stnu :activity-1)))
      (setf (upper-bound dur-1) 40))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))

    ;; The first activity's lower bound is now raised to violate the
    ;; time limit.
    (let ((dur-1 (find-temporal-constraint stnu :activity-1)))
      (setf (lower-bound dur-1) 70)
      (setf (upper-bound dur-1) 80))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-false controllable)
      (is-true neg-cycle)
      (verify-cycle neg-cycle
                    `((,(find-temporal-constraint stnu :activity-1) :lb)
                      (,(find-temporal-constraint stnu :activity-2) :lb)
                      (,(find-temporal-constraint stnu :time-limit) :ub))))))

;;; There are two uncontrollable durations in sequence. They must
;;; execute within 30 to 60 minutes. The controllability reductions
;;; will result in self-loops on the single controllable event.
(test self-loops
  (let ((stnu (make-instance 'temporal-network
                             :name :stnu-self-loops
                             :features
                             '(:simple-temporal-constraints
                               :simple-contingent-temporal-constraints))))

    ;; Both durations will take at most 30 minutes, but their
    ;; lower bounds would not meet the minimum requirement of
    ;; 30 minutes total.
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :id :activity-1
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 30)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :id :activity-2
                   :from-event :e2 :to-event :e3
                   :lower-bound 0 :upper-bound 30)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :time-limit
                   :from-event :e1 :to-event :e3
                   :lower-bound 30 :upper-bound 60)
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-false controllable)
      (is-true neg-cycle)
      (verify-cycle neg-cycle
                    `((,(find-temporal-constraint stnu :activity-1) :lb)
                      (,(find-temporal-constraint stnu :activity-2) :lb)
                      (,(find-temporal-constraint stnu :time-limit) :lb))))

    ;; Both duration's lower bounds are raised, so it just fits in
    ;; the allowed window of completion.
    (let ((dur-1 (find-temporal-constraint stnu :activity-1))
          (dur-2 (find-temporal-constraint stnu :activity-2)))
      (setf (lower-bound dur-1) 15)
      (setf (lower-bound dur-2) 15))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))

    ;; The second duration's window is translated up, thus violating
    ;; the maximum allowed time.
    (let ((dur-2 (find-temporal-constraint stnu :activity-2)))
      (setf (lower-bound dur-2) 25)
      (setf (upper-bound dur-2) 40))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-false controllable)
      (is-true neg-cycle)
      (verify-cycle neg-cycle
                    `((,(find-temporal-constraint stnu :activity-1) :ub)
                      (,(find-temporal-constraint stnu :activity-2) :ub)
                      (,(find-temporal-constraint stnu :time-limit) :ub))))

    ;; The first duration's upper bound is decreased to compensate.
    (let ((dur-1 (find-temporal-constraint stnu :activity-1)))
      (setf (upper-bound dur-1) 20))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))))

;;; There are two parallel threads of execution, each with two
;;; sequential activities. All activities have uncontrollable
;;; duration, and thus require a subsequent wait with commensurate
;;; controllability. There is a coordination constraint linking when
;;; thread 1's activity 2 starts with when thread 2's activity 2
;;; finishes and begins waiting.
(test cross-thread-constraint
  (let ((stnu (make-instance 'temporal-network
                             :name :stnu-cross-constraint
                             :features
                             '(:simple-temporal-constraints
                               :simple-contingent-temporal-constraints))))

    ;; All controllable waits may last forever. All uncontrollable
    ;; segments fall in [0, 50]. The cross-thread constraint allows
    ;; +/-25 minutes between its endpoints. This network is
    ;; controllable because of the infinite waits.
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :start-1
                   :from-event :start :to-event :e11
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :start-2
                   :from-event :start :to-event :e21
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :id :activity-1-1
                   :from-event :e11 :to-event :e12
                   :lower-bound 0 :upper-bound 50)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :wait-1-1
                   :from-event :e12 :to-event :e13
                   :lower-bound 0 :upper-bound +inf+)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :id :activity-1-2
                   :from-event :e13 :to-event :e14
                   :lower-bound 0 :upper-bound 50)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :wait-1-2
                   :from-event :e14 :to-event :e15
                   :lower-bound 0 :upper-bound +inf+)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :id :activity-2-1
                   :from-event :e21 :to-event :e22
                   :lower-bound 0 :upper-bound 50)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :wait-2-1
                   :from-event :e22 :to-event :e23
                   :lower-bound 0 :upper-bound +inf+)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :id :activity-2-2
                   :from-event :e23 :to-event :e24
                   :lower-bound 0 :upper-bound 50)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :wait-2-2
                   :from-event :e24 :to-event :e25
                   :lower-bound 0 :upper-bound +inf+)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :cross-constraint
                   :from-event :e13 :to-event :e24
                   :lower-bound -25 :upper-bound 25)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :end-1
                   :from-event :e15 :to-event :end
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'simple-temporal-constraint
                   :network stnu :id :end-2
                   :from-event :e25 :to-event :end
                   :lower-bound 0 :upper-bound 0)
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))

    ;; When the waits on the first activities of both threads' are
    ;; limited to 50 minutes, it forces the second activities to start
    ;; together at t = 50. But the second thread's activity may
    ;; last up to 50 minutes, which is 25-minutes past the cross-
    ;; constraint's limit.
    (let ((wait-1-1 (find-temporal-constraint stnu :wait-1-1))
          (wait-2-1 (find-temporal-constraint stnu :wait-2-1)))
      (setf (upper-bound wait-1-1) 50)
      (setf (upper-bound wait-2-1) 50))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-false controllable)
      (is-true neg-cycle)
      (verify-cycle neg-cycle
                    `((,(find-temporal-constraint stnu :start-1) :ub)
                      (,(find-temporal-constraint stnu :activity-1-1) :lb)
                      (,(find-temporal-constraint stnu :wait-1-1) :ub)
                      (,(find-temporal-constraint stnu :cross-constraint) :ub)
                      (,(find-temporal-constraint stnu :activity-2-2) :ub)
                      (,(find-temporal-constraint stnu :wait-2-1) :lb)
                      (,(find-temporal-constraint stnu :activity-2-1) :ub)
                      (,(find-temporal-constraint stnu :start-2) :lb))))

    ;; If the waits are allowed 75 minutes instead, we can wait the
    ;; extra 25 minutes needed on the first thread.
    (let ((wait-1-1 (find-temporal-constraint stnu :wait-1-1))
          (wait-2-1 (find-temporal-constraint stnu :wait-2-1)))
      (setf (upper-bound wait-1-1) 75)
      (setf (upper-bound wait-2-1) 75))
    (multiple-value-bind (controllable neg-cycle)
        (strongly-controllable? stnu)
      (is-true controllable)
      (is-false neg-cycle))))
