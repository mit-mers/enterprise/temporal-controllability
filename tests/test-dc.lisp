(in-package :temporal-controllability/test)
(in-suite :temporal-controllability/dc)

(defparameter *verbose-print-conflict* nil)

(defun print-conflict (conflict)
  (when *verbose-print-conflict*
    (if (null conflict)
        (format t "~%No conflict!")
        (format t "~%========================================~%~s~%"
                  (temporal-conflict-cycles conflict)))))

(test dc-test-basic-1

  ;; e1 == e2 -- e3
  ;;  \-------/

  ;; currently does not return an extension path for the negative cycle

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 20 :upper-bound 30)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 40 :upper-bound 45)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 50)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-2

  ;; e1 == e2 -- e3
  ;;  \-------/

  ;; currently does not return an extension path for the negative cycle

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 5 :upper-bound 30)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 40 :upper-bound 45)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 50)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-3

  ;;     e3
  ;;       \
  ;; e1 == e2

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 5 :upper-bound 30)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 1 :upper-bound 1)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-4

  ;; e1 == e2 -- e3 == e4 -- e5 == e6 -- e7
  ;;       \-----------------------------/

  ;; currently does not return an extension path for the n4 algorithm

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 3 :upper-bound 1000000)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound -1 :upper-bound 100000)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 1 :upper-bound 5.5)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e4 :to-event :e5
                   :lower-bound 0 :upper-bound 100000)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e5 :to-event :e6
                   :lower-bound 10 :upper-bound 14.5)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e6 :to-event :e7
                   :lower-bound 0 :upper-bound 100000)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e7
                   :lower-bound 5 :upper-bound 18)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-5

  ;; e1 => e2 <- e3
  ;;  \=========>/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 10)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 1 :upper-bound 8)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-6

  ;; e1 => e2 <- e3
  ;;  \========>/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 10)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 8)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-7

  ;; e1 => e2 <- e3
  ;;  \-------->/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 10)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 1 :upper-bound 8)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-8

  ;; e1 -- e2 -- e3
  ;;  \==========/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 8 )
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 1 :upper-bound 10)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-9

  ;; e1 -- e2 -- e3 -- e4
  ;;  \     \          /
  ;;   \====e5        /
  ;;    \------------/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e5
                   :lower-bound 0.6294 :upper-bound 18.8554)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e5
                   :lower-bound 0 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 1 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 1.5 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e4
                   :lower-bound 0 :upper-bound 3.5)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-10

  ;;  /-----\
  ;; e1 === e2
  ;;  \-----/

  ;; Tests to make sure multiple constraints over a single path don't mask
  ;; each other

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 3 :upper-bound 3.5)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 4 :upper-bound 6)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 2 :upper-bound 3.5)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-basic-11

  ;; e5 ---------\
  ;;      e1 === e2
  ;;             /
  ;;     e3 === e4

  ;; The n4 algorithm currently returns a true passed? value for a not
  ;; dynamically controllable stnu

  ;; Tests n4 algorithm for correctness bug and tests n3 algorithm for
  ;; conflict extraction bug

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 0 :upper-bound 3)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e4 :to-event :e2
                   :lower-bound -1 :upper-bound 3)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e5 :to-event :e2
                   :lower-bound 2 :upper-bound 4)
    ;; TODO: n^4 algorithm doesn't work. It has been removed from this test,
    ;; but the algorithm needs to be fixed and the test restored.
    (loop for algorithm in (list :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-morris-bug

  ;; e1 == e2
  ;;  \\     \
  ;;   e3 -- e4

  ;; one of the extension paths has an extra lowercase edge that isn't needed

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 1)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 1)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e4
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 0 :upper-bound 0)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-false controllablep)
            (is (not (null conflict)))))))

(test dc-test-controllable-network ()

  ;;  e1 --- e2 --- e3
  ;;  \\============//

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 4)
    (loop for algorithm in (list :n4 :n3) do
          (multiple-value-bind (controllablep conflict)
              (dynamically-controllable? stnu :algorithm algorithm)
            (is-true controllablep)
            (is (null conflict))))))
