(in-package :temporal-controllability/test)
(in-suite :temporal-controllability/delay)

(test delay-basic
  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 20 :observation-delay 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 0 :upper-bound 10)
    (is (not (delay-controllable? stnu)))))

(test delay-tiago-example-uncontrollable

  ;; e1 == e2 -- e3
  ;;             /
  ;;     e4 --- /

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 30 :observation-delay 15.1)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 20 :upper-bound 20)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e4 :to-event :e3
                   :lower-bound 5 :upper-bound 5)
    (is (not (delay-controllable? stnu)))))

(test delay-tiago-example-controllable

  ;; e1 == e2 -- e3
  ;;             /
  ;;     e4 --- /

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 30 :observation-delay 15)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 20 :upper-bound 20)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e4 :to-event :e3
                   :lower-bound 5 :upper-bound 5)
    (is (delay-controllable? stnu))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tests to emulate dynamic controllability
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test delay-dc-test-basic-1

  ;; e1 == e2 -- e3
  ;;  \-------/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 20 :upper-bound 30)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 40 :upper-bound 45)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 50)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-2

  ;; e1 == e2 -- e3
  ;;  \-------/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 5 :upper-bound 30)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 40 :upper-bound 45)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 50)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-3

  ;;     e3
  ;;       \
  ;; e1 == e2

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 5 :upper-bound 30)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 1 :upper-bound 1)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-4

  ;; e1 == e2 -- e3 == e4 -- e5 == e6 -- e7
  ;;       \-----------------------------/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 3 :upper-bound 1000000)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound -1 :upper-bound 100000)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 1 :upper-bound 5.5)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e4 :to-event :e5
                   :lower-bound 0 :upper-bound 100000)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e5 :to-event :e6
                   :lower-bound 10 :upper-bound 14.5)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e6 :to-event :e7
                   :lower-bound 0 :upper-bound 100000)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e7
                   :lower-bound 5 :upper-bound 18)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-5

  ;; e1 => e2 <- e3
  ;;  \=========>/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 10)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 1 :upper-bound 8)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-6

  ;; e1 => e2 <- e3
  ;;  \========>/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 10)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 8)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-7

  ;; e1 => e2 <- e3
  ;;  \-------->/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 10)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e2
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 1 :upper-bound 8)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-8

  ;; e1 -- e2 -- e3
  ;;  \==========/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 8 )
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 1 :upper-bound 10)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-9

  ;; e1 -- e2 -- e3 -- e4
  ;;  \     \          /
  ;;   \====e5        /
  ;;    \------------/

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e5
                   :lower-bound 0.6294 :upper-bound 18.8554)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e5
                   :lower-bound 0 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 1 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 1.5 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e4
                   :lower-bound 0 :upper-bound 3.5)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-basic-10

  ;;  /-----\
  ;; e1 === e2
  ;;  \-----/

  ;; Tests to make sure multiple constraints over a single path don't mask
  ;; each other

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 3 :upper-bound 3.5)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 4 :upper-bound 6)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 2 :upper-bound 3.5)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-morris-bug

  ;; e1 == e2
  ;;  \\     \\
  ;;   e3 -- e4

  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 1)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 1)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e4
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e3 :to-event :e4
                   :lower-bound 0 :upper-bound 0)
    (is (not (delay-controllable? stnu)))))

(test delay-dc-test-controllable-network
  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 4)
    (is (delay-controllable? stnu))))

(test delay-uncontrollable-example
  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint
                   :network stnu
                   :from-event :e1 :to-event :e2
                   :lower-bound 20 :upper-bound 40 :observation-delay 40)
    (make-instance 'tn:simple-temporal-constraint
                   :network stnu
                   :from-event :e2 :to-event :e3
                   :lower-bound -1 :upper-bound 14)
    (is (not (delay-controllable? stnu)))))
