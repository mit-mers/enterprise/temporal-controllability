(in-package :temporal-controllability)

(defun basic-test ()
  (let ((stnu (make-instance 'temporal-network
                             :features '(:simple-temporal-constraints
                                         :simple-contingent-temporal-constraints))))

    ;;create a simple, barely uncontrollable stnu
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 5)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 10)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :from-event :e2 :to-event :e4
                   :lower-bound 3 :upper-bound 8)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e3 :to-event :e5
                   :lower-bound 3 :upper-bound 5)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e4 :to-event :e6
                   :lower-bound 0 :upper-bound 1)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e5 :to-event :e6
                   :lower-bound 0 :upper-bound 1)
    (format t "~a" stnu)
    (multiple-value-bind (passed? conflicts)
        (dynamically-controllable?-n4 stnu)
      (dolist (cycle (temporal-conflict-cycles conflicts))
        (format t "~%new cycle")
        (loop for key being the hash-keys of (negative-cycle-constraints-ratio-map cycle) using (hash-value value) do
             (print key)
             (print value))))))

(defun advanced-loop ()
  (let ((stnu (make-instance 'temporal-network
                             :features '(:simple-temporal-constraints
                                         :simple-contingent-temporal-constraints))))
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 2)
    (make-instance 'simple-contingent-temporal-constraint
                   :network stnu :from-event :e3 :to-event :e4
                   :lower-bound 0 :upper-bound 3)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e4 :to-event :e2
                   :lower-bound -1 :upper-bound 3)
    (make-instance 'simple-temporal-constraint
                   :network stnu :from-event :e5 :to-event :e2
                   :lower-bound 2 :upper-bound 4)
    (format t "n3 algorithm~%")
    (multiple-value-bind (passed? conflicts)
        (dynamically-controllable?-n3 stnu)
      (format t "~a~%" passed?)
      (dolist (cycle (temporal-conflict-cycles conflicts))
        (format t "~%new cycle~%")
        (dolist (constraint (negative-cycle-constraints cycle))
          (format t "~a~%" constraint)))
      (format t "~a~%" passed?))
    (format t "n4 algorithm~%")
    (multiple-value-bind (passed? conflicts)
        (dynamically-controllable?-n4 stnu)
      (format t "~a~%" passed?)
      (dolist (cycle (temporal-conflict-cycles conflicts))
        (format t "~%new cycle~%")
        (dolist (constraint (negative-cycle-constraints cycle))
          (format t "~a~%" constraint))))))

(defun extension-checker ()
  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn)))
    (make-instance 'tn:simple-contingent-temporal-constraint :network stnu :from-event :e1 :to-event :e2
                   :lower-bound 3 :upper-bound 3.5)
    (make-instance 'tn:simple-temporal-constraint :network stnu :from-event :e1 :to-event :e2
                   :lower-bound 4 :upper-bound 6)
    (make-instance 'tn:simple-temporal-constraint :network stnu :from-event :e1 :to-event :e2
                   :lower-bound 2 :upper-bound 3.5)
    (multiple-value-bind (passed? conflicts)
        (dynamically-controllable?-n3 stnu)
      (format t "n3 algorithm~%")
      (format t "~a~%" passed?)
      (dolist (cycle (temporal-conflict-cycles conflicts))
        (format t "~%new cycle~%")
        (loop for key being the hash-keys of (negative-cycle-constraints-ratio-map cycle) using (hash-value value) do
          (print key)
          (print value))
        (dolist (constraint (negative-cycle-constraints cycle))
          (format t "~a~%" constraint)))
      (format t "~a~%" passed?))
    (format t "~%n4 algorithm~%")
    (multiple-value-bind (passed? conflicts)
        (dynamically-controllable?-n4 stnu)
      (format t "~a~%" passed?)
      (dolist (cycle (temporal-conflict-cycles conflicts))
        (format t "~%new cycle~%")
        (dolist (constraint (negative-cycle-constraints cycle))
          (format t "~a~%" constraint))))))
