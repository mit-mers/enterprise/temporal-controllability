;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package #:temporal-controllability)


;; PARSING ROUTINES
(defun read-temporal-network-file (temporal-network-filename)

  "Read in & parse a temporal network file"

  (let* ((temporal-network-string (alexandria:read-file-into-string (pathname temporal-network-filename))))
    (parse-temporal-network-string temporal-network-string)))

(defun parse-temporal-network-string (tn-string)

  "Parse the temporal network from an XML string"

  (let* ((tn-lxml (s-xml:parse-xml-string tn-string))
         (temporal-network (make-stnu 'new-stnu))
         (event-id-ht (make-hash-table :test 'equal))
         (events nil)
         (constraints nil))

    ;; Make sure the first tag is a TPN or CCTP
    (assert (or (is-tag? tn-lxml :cctp)
                (is-tag? tn-lxml :tpn)))

    ;; Extract events
    (loop for tag in (tag-body tn-lxml) do
         (when (is-tag? tag :event)

           (multiple-value-bind (id event) (parse-event tag)
             (push event events)
             (setf (gethash id event-id-ht) event))))


    ;; Extract constraints
    (loop for tag in (tag-body tn-lxml) do
         (when (is-tag? tag :constraint)
           (push (parse-constraint tag event-id-ht) constraints)))


    (loop for constraint in constraints do
         (add-temporal-constraint! temporal-network constraint))

    ;; Return the temporal network
    temporal-network))




;; PARSING ROUTINES
(defun read-ccpstp-file (filename &key (is-uniform? nil))

  "Read in & parse a ccpstp file"

  (let* ((ccpstp-string (alexandria:read-file-into-string (pathname filename) :multibyte t)))
    (parse-ccpstp-string ccpstp-string :is-uniform? is-uniform?)))


(defun parse-ccpstp-string (ccpstp-string &key (is-uniform? nil))

  "Parse the temporal network from an XML string"

  (let* ((tn-lxml (s-xml:parse-xml-string ccpstp-string))
         (temporal-network (make-pstn 'new-pstn))
         (event-id-ht (make-hash-table :test 'equal))
         (events nil)
         (constraints nil)
         (cc 0)
         (cc-relax-cost 0))

    ;; Make sure the first tag is a TPN or CCTP
    (assert (or (is-tag? tn-lxml :cctp)
                (is-tag? tn-lxml :tpn)))


    ;; Extract events
    (loop for tag in (tag-body tn-lxml) do

         (when (is-tag? tag :event)
           (multiple-value-bind (id event) (parse-event tag)
             (push event events)
             (setf (gethash id event-id-ht) event)))

       ;; Extract cc constraint and cc relaxation ratio

         (when (is-tag? tag :risk-bound)
           (setf cc (parse-number (second tag))))

         (when (is-tag? tag :risk-bound-relax-cost)
           (setf cc-relax-cost (parse-number (second tag)))))

    ;; Extract constraints
    (loop for tag in (tag-body tn-lxml) do
         (when (is-tag? tag :constraint)
           (push (parse-constraint tag event-id-ht :is-uniform? is-uniform?) constraints)))


    (loop for constraint in constraints do
         (add-temporal-constraint! temporal-network constraint))

    ;; Return the temporal network
    (values temporal-network cc cc-relax-cost)))




(defun parse-constraint (tag event-id-ht &key (is-uniform? nil))

  "Parse a constraint from LXML,
   based on the event object stored in the ht"

  (assert (is-tag? tag :constraint))

  (let* ((start-id nil)
         (end-id nil)
         (name nil)
         (lb nil)
         (ub nil)
         (mean 0)
         (variance 0)
         (controllable? t)
         (lb-relaxable? nil)
         (ub-relaxable? nil)
         (lb-relax-cost 0)
         (ub-relax-cost 0))

    (loop for entry in (tag-body tag) do
         (cond ((is-tag? entry :start)
                (setf start-id (second entry)))

               ((is-tag? entry :end)
                (setf end-id (second entry)))

               ((or (is-tag? entry :primitive)
                    (is-tag? entry :name))
                (setf name (second entry)))

               ((is-tag? entry :lowerbound)
                (setf lb (parse-number (second entry))))

               ((is-tag? entry :upperbound)
                (setf ub (parse-number (second entry))))

               ((is-tag? entry :mean)
                (setf mean (parse-number (second entry))))

               ((is-tag? entry :variance)
                (setf variance (parse-number (second entry))))

               ((is-tag? entry :type)
                (when (search "Uncontrollable" (second entry))
                  (setf controllable? nil)))

               ((is-tag? entry :lbrelaxable)
                (when (search "T" (second entry))
                  (setf lb-relaxable? t)))

               ((is-tag? entry :ubrelaxable)
                (when (search "T" (second entry))
                  (setf ub-relaxable? t)))

               ((is-tag? entry :lb-relax-cost-ratio)
                (setf lb-relax-cost (parse-number (second entry))))

               ((is-tag? entry :ub-relax-cost-ratio)
                (setf ub-relax-cost (parse-number (second entry))))))

    (if controllable?
        (make-temporal-constraint (gethash start-id event-id-ht)
                                  (gethash end-id event-id-ht)
                                  (make-bounded-temporal-duration lb ub
                                                                  :controllable? controllable?
                                                                  :lb-relaxable? lb-relaxable?
                                                                  :ub-relaxable? ub-relaxable?
                                                                  :lb-relax-cost lb-relax-cost
                                                                  :ub-relax-cost ub-relax-cost)
                                  name)

        (if is-uniform?
            (make-temporal-constraint (gethash start-id event-id-ht)
                                      (gethash end-id event-id-ht)
                                      (make-bounded-temporal-duration lb ub
                                                                      :controllable? controllable?)
                                      name)

            (make-temporal-constraint (gethash start-id event-id-ht)
                                      (gethash end-id event-id-ht)
                                      (make-probabilistic-temporal-duration
                                       (make-normal-distribution mean variance))
                                      name)))))

(defun parse-event (tag)

  "Parse an event from LXML,
   return two items: the id of the event
   and the event object"

  (assert (is-tag? tag :event))

  (let* ((event-name nil)
         (id nil))

    (loop for entry in (tag-body tag) do
         (cond ((is-tag? entry :ID)
                (setf id (second entry)))
               ((or (is-tag? entry :name)
                    (is-tag? entry :primitive))
                (setf event-name (second entry)))))

    (values id (make-instance 'temporal-event :name event-name))))



(defun gethash-or-error (key ht &key (error-text "Couldn't find \"~a\" in hash table!"))

  "A utility function that looks up something in a hash table, or throws an error
   if it can't be found."

  (let* ((obj nil)
         (found? nil))
    (multiple-value-setq (obj found?) (gethash key ht))
    (if found?
        obj
        (error (format nil error-text key)))))




(defun parse-number (s)

  "Parses a number. Might return infinity or negative infinity!"

  (let* ((a (string-downcase (string-trim " " s))))
    (cond ((or (equalp a "+inf")
               (equalp a "inf")
               (equalp a "+infinity")
               (equalp a "infinity"))
           +infinity+)

          ((or (equalp a "-inf")
               (equalp a "-infinity"))
           +-infinity+)

          (T
           (float (read-from-string a))))))


(defun parse-boolean (s)

  "Parses a boolean from a string. Converts True to T, False to nil."

  (let* ((a (string-downcase (string-trim " " s))))
    (cond ((or (equalp a "true")
               (equalp a "t")
               (equalp a "yes")
               (equalp a "y"))
           T)

          ((or (equalp a "false")
               (equalp a "f")
               (equalp a "no")
               (equalp a "n"))
           nil)

          (T
           (error (format nil "Can't convert this to a boolean: ~a" s))))))



;; Macro
(defmacro symbol-equal (s1 s2)
  `(equal (string-downcase (symbol-name ,s1))
          (string-downcase (symbol-name ,s2))))

(defmacro tag-name (tag)
  `(if (listp ,tag)
       (first ,tag)
       nil))

(defmacro tag-body (tag)
  `(cdr, tag))

(defmacro is-tag? (tag type)
  `(symbol-equal (tag-name ,tag)
                 ,type))
