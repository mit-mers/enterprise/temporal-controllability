;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defun dc-test-stdin ()
  ;; read in number of controllable edges
  (let ((stnu (tn:make-simple-temporal-network-with-uncertainty :test1-stn))
        (num_controllable nil)
        (num_uncontrollable nil))

    (defun convert-infinity (n)
      (cond ((eq n "Infinity") (setq n +infinity+))
            ((eq n "-Infinity") (setq n +-infinity+))
            ((eq n 'INFINITY) (setq n +infinity+))
            ((eq n '-INFINITY) (setq n +-infinity+)))
      n)

    (setq num_controllable (read))
    ;; read in number of uncontrollabl:continuee edges
    (dotimes (n num_controllable)
      (let* ((from_node (read))
             (to_node (read))
             (lb (read))
             (ub (read))
             (lb (convert-infinity lb))
             (ub (convert-infinity ub)))
        ;;(format t "lb:<~a>(~a) ub:<~a>(~a)~%" lb (type-of lb) ub (type-of ub))
        (make-instance 'tn:simple-temporal-constraint :network stnu :from-event from_node :to-event to_node
                       :lower-bound lb :upper-bound ub)))

    (setq num_uncontrollable (read))
    (dotimes (n num_uncontrollable)
      (let* ((from_node (read))
             (to_node (read))
             (lb (read))
             (ub (read))
             (lb (convert-infinity lb))
             (ub (convert-infinity ub)))
        (make-instance 'tn:simple-temporal-constraint :network stnu :from-event from_node :to-event to_node
                       :lower-bound lb :upper-bound ub)))

    (multiple-value-bind (controllable conflict)
        (dynamically-controllable? stnu)
      (declare (ignore conflict))
      (if controllable (print "dc") (print "notdc"))
      ())))

(defun dc-test-collection ()

  (let (;;(foldername
        ;;"C:/Users/yupeng/Dropbox/Code/Algorithms/incremental-dynamic-controllability/tests/")
        ;;(foldername
        ;;"E:/Dropbox/Code/Algorithms/incremental-dynamic-controllability/tests/")
        (foldername "/Users/yupeng/Dropbox/Code/Algorithms/incremental-dynamic-controllability/tests/")
        (filenames nil))

    (push "raw_xml/test1.xml" filenames)
    (push "raw_xml/test2.xml" filenames)
    (push "raw_xml/test3.xml" filenames)
    (push "raw_xml/test4.xml" filenames)
    (push "raw_xml/test5.xml" filenames)
    (push "raw_xml/test6.xml" filenames)
    (push "raw_xml/test7.xml" filenames)
    (push "raw_xml/test8.xml" filenames)
    (push "raw_xml/test9.xml" filenames)
    (push "raw_xml/test10.xml" filenames)
    (push "raw_xml/test11.xml" filenames)
    (push "raw_xml/test12.xml" filenames)
    (push "raw_xml/test13.xml" filenames)
    (push "raw_xml/test14.xml" filenames)
    (push "raw_xml/test15.xml" filenames)
    (push "raw_xml/test16.xml" filenames)
    (push "raw_xml/test17.xml" filenames)
    (push "raw_xml/test18.xml" filenames)
    (push "raw_xml/test19.xml" filenames)
    (push "raw_xml/test20.xml" filenames)
    (push "raw_xml/test21.xml" filenames)
    (push "raw_xml/test22.xml" filenames)
    (push "raw_xml/test23.xml" filenames)

    (dolist (filename filenames)
      (test-from-file foldername filename))))

(defun test-from-file (foldername filename)

  ;; Test if the parser works

  (let ((stnu (read-temporal-network-file (concatenate 'string foldername filename))))

    ;; (format t "Load TN from ~A with ~A constraints~%" filename (list-length (temporal-network-constraints stnu)))

    (multiple-value-bind (controllable conflict)
        (dynamically-controllable? stnu)

      (format t "Is-controllable? ~A~%" controllable)
      (when (not controllable)
        ;; Not controllable
        ;; print the conflict detected
        (print-temporal-conflict conflict)))))


(defun print-temporal-conflict (conflict)

  "Print the content of a temporal conflict"

  (if (eq conflict nil)
      (format t "conflicts are nil :(")
      (progn
        (format t "~%Conflict detected with ~A negative cycles.~%" (list-length (temporal-conflict-cycles conflict)))

        (let ((cycle-counter 0))

          (dolist (ncycle (temporal-conflict-cycles conflict))
            (incf cycle-counter)
            (format t "Cycle ~A; n-value: ~A;~%" cycle-counter (negative-cycle-value ncycle))

            (dolist (constraint-in-conflict (negative-cycle-constraints ncycle))
              (let* ((constraint (first constraint-in-conflict))
                     (start (from-event constraint))
                     (end (to-event constraint))
                     (lb (lower-bound constraint))
                     (ub (upper-bound constraint))
                     (bound (second constraint-in-conflict))
                     (sign (third constraint-in-conflict))
                     (reference (fourth constraint-in-conflict)))

                (format t "~A-->~A:[~A,~A](~A~A~A)~%" (name start) (name end)
                        lb ub bound sign reference))))))))
