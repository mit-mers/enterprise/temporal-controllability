;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defclass d-edge ()

  ((from-node
    :initform nil
    :initarg :from-node
    :accessor d-edge-from-node
    :documentation "the from node of the edge")

   (to-node
    :initform nil
    :initarg :to-node
    :accessor d-edge-to-node
    :documentation "the to node of the edge")

   (weight
    :initform nil
    :initarg :weight
    :accessor d-edge-weight
    :documentation "the weight of the edge")

   (conditional?
    :initform nil
    :initarg :conditional?
    :accessor d-edge-conditional?
    :documentation "if the edge is conditional")

   (conditioned-node
    :initform nil
    :initarg :conditioned-node
    :accessor d-edge-conditioned-node
    :documentation "the node that this edge is conditioned on")

   (is-upper-case?
    :initform nil
    :initarg :is-upper-case?
    :accessor d-edge-is-upper-case?
    :documentation "is the edge an upper case conditional one?"))


  (:documentation "a directed edge (maybe conditional) used in the equivalent distance graph of dynamic controllability problems"))

(defmethod d-edge-is-lower-case? ((d-edge d-edge))
  (and (d-edge-conditional? d-edge)
       (not (d-edge-is-upper-case? d-edge))))

(defmethod print-object ((d-edge d-edge) stream)
  (with-slots (from-node to-node weight conditioned-node is-upper-case?) d-edge
    (format stream
            "#<CDG-EDGE ~s ~s ~s label: ~s upper: ~s>"
            from-node to-node weight conditioned-node is-upper-case?)))


(defun create-edge (from to w
                    &key (c? nil)
                      (cnode nil)
                      (upper? nil))

  "Create a d-edge"

  (make-instance 'd-edge
                 :from-node from
                 :to-node to
                 :weight w
                 :conditional? c?
                 :conditioned-node cnode
                 :is-upper-case? upper?))
