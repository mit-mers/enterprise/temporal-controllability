(in-package :temporal-controllability)

(defstruct incremental-data-structures
  (cdg-conflicts nil)
  (completed-calls-dag nil)
  (graph nil)
  (negative-nodes nil)
  (novel-edges nil)
  (positive-map nil)
  (event-node-map nil)
  (shortest-path-predecessors nil))

(defun incremental-dc (edge-to-change new-weight ids)
  ""
  (incf (d-edge-weight edge-to-change) new-weight)
  (let* ((dag (incremental-data-structures-completed-calls-dag ids))
         (preprocess-order (reverse (topologically-sort-nodes dag)))
         (global-predecessors (incremental-data-structures-shortest-path-predecessors ids))
         (updated-from-node (d-edge-from-node edge-to-change))
         (graph (incremental-data-structures-graph ids))
         (negative-nodes (incremental-data-structures-negative-nodes ids))
         (novel-edges (incremental-data-structures-novel-edges ids))
         (positive-map (incremental-data-structures-positive-map ids))
         (event-node-map (incremental-data-structures-event-node-map ids))
         (changed (make-hash-table :test 'equal)))
    (loop for start-node in preprocess-order do
         (let ((predecessors (gethash start-node global-predecessors)))
           (if (or (equal (gethash updated-from-node predecessors) edge-to-change)
                   (any-changed? (gethash start-node dag) changed))
               (let ((graph-edges (list))
                     (old-predecessors (gethash start-node global-predecessors)))
                 (dolist (edge (gethash start-node graph))
                   (if (not (remhash edge novel-edges))
                       (setf graph-edges (cons edge graph-edges))))
                 (setf (gethash start-node graph) graph-edges)
                 (rebooting-dijkstra graph
                                     global-predecessors
                                     dag
                                     novel-edges
                                     start-node
                                     (list start-node)
                                     (make-hash-table :test 'equal)) ; don't need to worry about recursion since the DAG handles that
                 (if (hash-tables-differ old-predecessors (gethash start-node global-predecessors))
                     (setf (gethash start-node changed) t))))))
    (run-main-dc-n3 graph
                    global-predecessors
                    dag
                    novel-edges
                    negative-nodes
                    positive-map
                    event-node-map)))

(defun any-changed? (nodes changed)
  (loop for node being the hash-keys in nodes do
       (if (gethash node changed)
           (return-from any-changed? t)))
  nil)

(defun hash-tables-differ (h1 h2)
  (if (= (hash-table-count h1)
         (hash-table-count h2))
      (progn
        (loop for node being the hash-keys in h1 using (hash-value edge) do
             (if (not (equal edge (gethash node h2)))
                 (return-from hash-tables-differ t)))
        nil)
      t))

(defun topologically-sort-nodes (dag)
  (let ((marked (make-hash-table :test 'equal))
        (output (list)))
    (loop for node being the hash-keys in dag do
         (setf output (visit-topological-sort node dag marked output)))
    output))

(defun visit-topological-sort (node dag marked output)
  (if (gethash node marked)
      output
      (progn
        (setf (gethash node marked) t)
        (loop for next being the hash-keys in (gethash node dag) do
             (setf output (visit-topological-sort next dag marked output)))
        (cons node output))))
