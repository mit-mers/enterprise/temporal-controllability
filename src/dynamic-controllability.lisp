;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defun dynamically-controllable? (stnu &key (algorithm nil))
  (cond ((eql algorithm :n4) (dynamically-controllable?-n4 stnu))
        (t (dynamically-controllable?-n3 stnu))))

(defun dynamically-controllable?-n4 (stnu)

  "Check the dynamic controllability of the stnu, returning a conflict.
   This function implements the DC checking algorithm from (Morris 2006),
   an O(N4) algorithm, with conflict extraction procedures from
   (Yu, Fang and Williams, 2014).

   Input: An STNU.

   Output: a boolean 'controllable', and a conflict of a disjunctive set of negative cycles,
   nil if the network is dynamically controllable"

  ;; First, we generate a normalized distance graph of the STNU;
  ;; See (Morris 2006)
  ;; The distance graph has contingent edges
  ;; we got several things back from the mapping function
  ;; cdg: a set that contains edges in the equivalent distance graph,
  ;;      with upper case conditional edges
  ;; lower-edges: the set of edges that are lower case conditional
  ;; positive-map: mapping between distance edges and constraints
  ;; negative-map: mapping between distance edges and
  ;;      'negatively' supported edges.

  (multiple-value-bind (stn cdg lower-edges positive-map)
      (get-normalized-mapping stnu)

    ;; (print-edge-set lower-edges) (print-edge-set cdg)

    ;; Second, run the fastDC algorithm to check dynamic
    ;; controllability.
    (fastDC cdg lower-edges positive-map stn)))


(defun fastDC (cdg lower-edges positive-map stn)

  "This function implements the fastDC algorithm introduced in
   (Morris 2006), with modifications for conflict extraction"

  ;; Assemble an initial STN for ITC, based on the
  ;; edges in cdg (lower case edges are ignored in allMax checking)


  (multiple-value-bind (stn-cdg-map cdg-stn-map)
      (get-cdg-stn-mapping cdg stn)

    (let ((itc (make-itc stn :itc-method :negative-cycle-detection))
          (reduced-edges nil))

      ;; then proceed to reduction

      (loop for i from 1 to (list-length lower-edges) do
         ;; if no reduction is made in this iteration
         ;; stop and return
           (let ((found-reduction nil))

             ;; First, check allMax consistency before doing any reduction
             (multiple-value-bind (consistent neg-cycle)
                 (temporal-network-consistent? itc)

               ;; return if a conflict is detected
               (when (not consistent)
                 (return-from fastDC (values nil (extract-conflict neg-cycle reduced-edges stn-cdg-map positive-map)))))

             ;; Next loop through all lower case edges
             ;; to look for moat paths
             (dolist (lower-case-edge lower-edges)
               ;; (format t "looking for moat path from ")
               ;; (print-edge lower-case-edge)
               ;; (format t "~%")

               ;; Find all most paths from this lower edge
               (let ((moat-paths (find-moat-paths lower-case-edge cdg)))
                 (dolist (moat-path moat-paths)

                   ;; (format t "found moat path~%")
                   ;; (print-edge lower-case-edge)
                   ;; (format t "~%")
                   ;; (print-edge-set moat-path)

                   ;; if a moat path is found,
                   ;; we will be able to add some new edges
                   ;; Note that a moat path is simply a list of d-edges

                   ;; reduce the path to an edge
                   ;; then try to add it to the cdg
                   (let* ((reduced-edge (reduce-path lower-case-edge moat-path))
                          (added (add-reduced-edge reduced-edge cdg stn stn-cdg-map cdg-stn-map)))

                     ;; If this path can be reduced and added
                     ;; we set the indicator to true, which asks for
                     ;; at least one more iteration of reduction

                     ;; (format t "reduced to ")
                     ;; (print-edge reduced-edge)
                     ;; (format t "  ~A~%" added)

                     (when added
                       (setf found-reduction t)
                       (push reduced-edge reduced-edges)

                       ;; collect the supporting constraints for the reduced edge

                       (let ((positive-supports nil))
                         (dolist (edge moat-path)
                           (setf positive-supports (append positive-supports (gethash edge positive-map))))
                         ;; attach it to the new constraints
                         (setf (gethash reduced-edge positive-map) positive-supports)))))))

             (when (not found-reduction)
               (return))))

      ;; Finally, check allMax consistency again before finishing
      ;; in case we have not done this before (due to no lower case edges)
      (multiple-value-bind (consistent neg-cycle)
          (temporal-network-consistent? itc)
        (if consistent
            (values t nil)
            (values nil (extract-conflict neg-cycle reduced-edges stn-cdg-map positive-map)))))))

(defun reduce-path (lower-edge moat-path)

  "Reduce a moat path for a lower edge,
   return a reduced edge, or nil if the reduction
   cannot be completed."

  ;; first, the path must be valid
  (when (not moat-path)
    (return-from reduce-path nil))

  ;; start reduction
  ;; Note that the reduction is divided into two steps
  (let ((reduced-edge (pop moat-path)))

    ;; First, we reduce the moat path (no lower edge)
    ;; only upper case, no case and label removal reduction are needed
    (loop while (> (list-length moat-path) 0) do
         (setf reduced-edge (reduce-edges reduced-edge (pop moat-path)))
         (when (not reduced-edge)
           (return-from reduce-path nil)))

    ;; Second, we reduce the lower edge with the reduced path
    ;; we may need lower case and cross case here
    (reduce-edges lower-edge reduced-edge)))


(defun reduce-edges (start-edge end-edge)

  "Applying reduction rules in Morris, 2006
   to reduce two edges into one.
   return nil if no reduction rule applies"

  (let ((start-node (d-edge-from-node start-edge))
        (end-node (d-edge-to-node end-edge))
        (start-weight (d-edge-weight start-edge))
        (end-weight (d-edge-weight end-edge)))

    ;; upper case
    (when (and (not (d-edge-conditional? start-edge))
               (d-edge-is-upper-case? end-edge))
      ;; (format t "Upper Case~%")
      (return-from reduce-edges (create-edge start-node end-node
                                             (+ start-weight end-weight)
                                             :c? t :cnode (d-edge-conditioned-node end-edge) :upper? t)))

    ;; label removal
    (when (and (d-edge-is-upper-case? start-edge)
               (>= start-weight 0))
      ;; (format t "Label removal~%")
      (let* ((label-removed-edge (create-edge start-node (d-edge-to-node start-edge) start-weight))
             (reduced-edge (reduce-edge label-removed-edge end-edge)))
        (return-from reduce-edges reduced-edge)))

    ;; cross case
    (when (and (not (d-edge-is-upper-case? start-edge))
               (d-edge-is-upper-case? end-edge)
               (< end-weight 0)
               (not (equal (d-edge-conditioned-node start-edge) (d-edge-conditioned-node end-edge))))
      ;; (format t "Cross Case~%")
      (return-from reduce-edges (create-edge start-node end-node
                                             (+ start-weight end-weight)
                                             :c? t :cnode (d-edge-conditioned-node end-edge) :upper? t)))

    ;; lower case
    (when (and (d-edge-conditional? start-edge)
               (not (d-edge-is-upper-case? start-edge))
               (not (d-edge-conditional? end-edge))
               (< end-weight 0))
      ;; (format t "Lower Case~%")
      (return-from reduce-edges (create-edge start-node end-node (+ start-weight end-weight))))

    ;; no case
    (when (and (not (d-edge-conditional? start-edge))
               (not (d-edge-conditional? end-edge)))
      ;; (format t "No Case~%")
      (return-from reduce-edges (create-edge start-node end-node (+ start-weight end-weight))))

    nil))


(defun find-moat-paths (lower-edge cdg)

  "Find all negative paths (moat paths) from the lower-edge"

  ;; The key idea is to run a single source shortest path
  ;; algorithm from the source (end of the lower-edge)
  ;; then extract all paths to the node with negative weight

  ;; Here we use Bellman ford algorithm

  (let ((moat-paths nil)
        (source-node (d-edge-to-node lower-edge))
        (conditional-node (d-edge-conditioned-node lower-edge))
        (predecessor-map (make-hash-table :test 'equal))
        (node-distance-map (make-hash-table :test 'equal)))

    ;; First, we initialize the distances of nodes in the cdg
    ;; the end node of the lower edge got assigned zero
    ;; all others are assigned inf

    ;; (format t "Initializing node distance hashmap from ~A edges~%" (list-length cdg))

    (dolist (edge cdg)
      (setf (gethash (d-edge-from-node edge) node-distance-map) +inf+)
      (setf (gethash (d-edge-to-node edge) node-distance-map) +inf+))

    (setf (gethash source-node node-distance-map) 0)

    ;; Second, run bellman ford to get a distance map from source to all other nodes

    ;; (format t "Running bellman ford to update node distance map~%")

    (loop for i from 1 to (hash-table-count node-distance-map) do
       ;; (format t "Bellman Ford ~A/~A~%" i (hash-table-count node-distance-map))
         (dolist (edge cdg)
           (let* ((start-node (d-edge-from-node edge))
                  (end-node (d-edge-to-node edge))
                  (dist-start (gethash start-node node-distance-map))
                  (dist-end (gethash end-node node-distance-map))
                  (edge-weight (d-edge-weight edge))
                  (predecessors nil))

             (when (not (equal conditional-node (d-edge-conditioned-node edge)))
               ;; make update if we find a lower weight path
               ;; (to nodes other than the source node)
               (when (< (+ dist-start edge-weight) dist-end)
                 (setf (gethash end-node node-distance-map) (+ dist-start edge-weight))
                 (push edge predecessors)
                 (setf (gethash end-node predecessor-map) predecessors))

               ;; if the weight is the same (and not inf), add edge to the predecessor map
               (when (and (not (equal dist-end +inf+)) (= (+ dist-start edge-weight) dist-end))
                 (setf predecessors (gethash end-node predecessor-map))
                 (when (not (member edge predecessors))
                   (push edge predecessors)
                   (setf (gethash end-node predecessor-map) predecessors)))))))

    ;; Third, go through all node with negative weight, and extract path to source
    (loop for node being the hash-keys of node-distance-map using (hash-value distance) do
         (when (and (< distance 0) (gethash node predecessor-map))
           ;; Ok, we got a node with negative distance
           ;; Next, extract the path from source to it
           ;; note that no cycle is allowed in the path
           ;; and the end edge must be a moat (negative)

           ;; (format t "Node ~A has negative distance ~A~%" node distance)

           (let ((candidate-moat-path nil)
                 (candidate-path-queue nil))
             (dolist (moat-edge (gethash node predecessor-map))
               ;; in addition to negative end edge,
               ;; the from node of the moat edge must have a non-negative
               ;; distance.
               (when (and (< (d-edge-weight moat-edge) 0)
                          (>= (gethash (d-edge-from-node moat-edge) node-distance-map) 0))
                 (let ((new-moat-path (list moat-edge)))
                   (push new-moat-path candidate-path-queue))))

             ;; We may have got a set of candidate paths (with only moat edges)
             ;; Next we try to trace back using the predecessor map
             ;; Keep trying until we run out of candidate, or found a feasible path
             (loop while (and candidate-path-queue (not candidate-moat-path)) do
                ;; Retrive one partial path in the queue
                ;; and its leading edge
                  (let* ((partial-moat-path (pop candidate-path-queue))
                         (leading-edge (first partial-moat-path))
                         (leading-node (d-edge-from-node leading-edge))
                         (parent-edges (gethash leading-node predecessor-map)))

                    ;; (format t "Queue length ~A~%" (list-length candidate-path-queue))
                    ;; (format t "Start Extending Path of ~A~%" (list-length partial-moat-path))

                    ;; if the from node of the leading edge is the start
                    ;; end the loop and return
                    (when (equal (d-edge-to-node lower-edge) leading-node)
                      (setf candidate-moat-path partial-moat-path)
                      (return))

                    ;; Otherwise let's keep tracing
                    (when parent-edges
                      (dolist (parent-edge parent-edges)
                        (let ((skip-this-edge nil))

                          ;; We cannot have lower edge (guaranteed by the cdg)
                          ;; (we store all lower case edges in a separate list)

                          ;; We cannot have cycle
                          (dolist (edge partial-moat-path)
                            (when (equal (d-edge-from-node parent-edge) (d-edge-to-node edge))
                              ;; A cycle is detected
                              ;; this parent-edge cannot be used for extension
                              (setf skip-this-edge t)
                              (return)))

                          ;; One more thing to check
                          ;; there should not be any subpath in the candidate path with less than zero weight
                          ;; That is to say, the distance of the from node must be positive
                          (when (< (gethash (d-edge-from-node parent-edge) node-distance-map) 0)
                            (setf skip-this-edge t))

                          ;; If this parent-edge is valid,
                          ;; We use it to extend the partial moat path
                          ;; and add the new partial path back to the queue
                          (when (not skip-this-edge)
                            (let ((new-partial-path (append (list parent-edge) partial-moat-path)))

                              ;; (format t "Adding path of ~A to the queue ~A~%" (list-length new-partial-path)
                              ;;      (list-length candidate-path-queue))
                              ;; (print-edge parent-edge)
                              ;; (format t "++")
                              ;; (print-edge-set partial-moat-path)

                              (setf candidate-path-queue (append candidate-path-queue (list new-partial-path))))))))))

             (when candidate-moat-path
               (push candidate-moat-path moat-paths)))))

    (values moat-paths)))


(defun extract-conflict (neg-cycle reduced-edges stn-cdg-map positive-map)

  "Given a negative cycle, map it back to constraints and durations in
   the original problem, and generate a conflict"

  (let ((conflict (create-temporal-conflict))
        (ncycle (create-negative-cycle))
        (nvalue 0))

    ;; iterate through all stn-edges
    ;; map them back to d-edges
    ;; then to constraints in the STNU

    ;; (format t "Found ncycle with ~A edges.~%" (list-length neg-cycle))

    (dolist (stn-edge neg-cycle)
      (let* ((d-edge (gethash (first stn-edge) stn-cdg-map))
             (weight (d-edge-weight d-edge))
             (positive-supports (gethash d-edge positive-map)))

        ;; (print-edge d-edge)
        ;; (format t "  is-reduced ~A  " (member d-edge reduced-edges))
        ;; (format t "~%")

        ;; add positive supports to the negative cycle
        (dolist (support positive-supports)
          (add-constraint-to-cycle ncycle support))

        ;; update the negative value
        (incf nvalue weight)

        ;; if this edge is a reduced one (does not belong to the original network)
        ;; we may resolve the conflict by disabling the reduction
        ;; basically, to apply relaxation so that the weight becomes zero
        ;; this requires an additional ncycle
        ;; with all positive supports
        ;; and a negative value equal to the weight
        (when (member d-edge reduced-edges)

          (let ((reduced-edge-ncycle (create-negative-cycle)))
            (dolist (reduced-edge-support positive-supports)
              (add-constraint-to-cycle reduced-edge-ncycle reduced-edge-support))
            (setf (negative-cycle-value reduced-edge-ncycle) weight)
            (add-cycle-to-conflict conflict reduced-edge-ncycle)))))

    (setf (negative-cycle-value ncycle) nvalue)
    (add-cycle-to-conflict conflict ncycle)
    (values conflict)))


(defun add-reduced-edge (new-edge cdg stn stn-cdg-map cdg-stn-map)

  "Add a reduced edge to the network (if qualified).
   If yes, return t; if not, return nil"

  ;; first, if the new edge is nil
  ;; return nil
  (when (not new-edge)
    (return-from add-reduced-edge nil))

  ;; (format t "Adding edge ")
  ;; (print-edge new-edge)
  ;; (format t "~%")

  (let ((to-be-added? t)
        (edges-to-be-removed nil)
        (start (d-edge-from-node new-edge))
        (end (d-edge-to-node new-edge))
        (weight (d-edge-weight new-edge))
        (conditional? (d-edge-conditional? new-edge)))

    ;; second, is there a same type d-edge at the same location?
    (dolist (edge cdg)
      (when (and (equal start (d-edge-from-node edge))
                 (equal end (d-edge-to-node edge))
                 (equal conditional? (d-edge-conditional? edge)))

        ;; if yes, compare their weight
        ;; if the new edge's weight is smaller, keep it
        ;; remove the old edge from cdg
        ;; and from the stn

        ;; (format t "found similar edge ")
        ;; (print-edge edge)
        ;; (format t "~%")

        (if (< weight (d-edge-weight edge))
            (push edge edges-to-be-removed)
            (setf to-be-added? nil))))

    ;; remove edges that are subsumed by the new one
    (dolist (edge-to-remove edges-to-be-removed)
      (setf cdg (remove edge-to-remove cdg))
      (remove-temporal-constraint! stn (gethash edge-to-remove cdg-stn-map)))

    ;; add the new edge to the cdg and stn
    (when to-be-added?
      (let* ((new-constraint
              (make-instance 'tn:simple-temporal-constraint :network stn
                             :from-event start :to-event end
                             :lower-bound +-inf+ :upper-bound (d-edge-weight new-edge))))

        (push new-edge cdg)
        (setf (gethash new-constraint stn-cdg-map) new-edge)
        (setf (gethash new-edge cdg-stn-map) new-constraint)
        (return-from add-reduced-edge t))))

  nil)


(defun get-cdg-stn-mapping (cdg stn)

  "Given a list of conditional d-edges, return the mappings from constraints to
   conditional edges and vice versa."

  (let ((stn-cdg-map (make-hash-table :test 'equal))
        (cdg-stn-map (make-hash-table :test 'equal)))
    (dolist (d-edge cdg)
      (let* ((start (d-edge-from-node d-edge))
             (end (d-edge-to-node d-edge))
             (new-constraint
              (make-instance 'tn:simple-temporal-constraint :network stn
                             :from-event start :to-event end
                             :lower-bound +-inf+ :upper-bound (d-edge-weight d-edge))))

        (setf (gethash new-constraint stn-cdg-map) d-edge)
        (setf (gethash d-edge cdg-stn-map) new-constraint)))

    (values stn-cdg-map cdg-stn-map)))


(defun get-simplified-mapping (stnu)
  "Generate the equivalent distance graph given an stnu.

   Input: An STNU.

   Output:
   cdg: a set that contains edges in the equivalent distance graph,
        with upper case conditional edges
   lower-edges: the set of edges that are lower case conditional
   gamma: mapping between contingent constraints and observation delay"

   (let* ((cdg nil)
          (lower-edges nil)
          (stn (make-simple-temporal-network (gensym "CDG-EQV-STN-")))
          (gamma (make-hash-table :test 'equal)))
    (do-constraints (constraint stnu)

      ;; (let* ((start (temporal-constraint-start constraint))
      ;;        (end (temporal-constraint-end constraint))
      ;;        (duration (temporal-constraint-duration constraint)))

      ;;   (format t "~A-->~A:[~A,~A]  ~A~%" (event-name start) (event-name end)
      ;;           (duration-lower-bound duration) (duration-upper-bound duration) (temporal-duration-controllable? duration)))

      (let ((start-node (from-event constraint))
            (end-node (to-event constraint))
            (lb (lower-bound constraint))
            (ub (upper-bound constraint)))

        (if (not (typep constraint 'simple-temporal-constraint))

            ;; an uncontrollable constraint
            (progn

              ;; we need to generate six edges
              ;; four unconditional plus two conditional
              (let* ((mid-node (make-instance 'temporal-event :network stn))
                     (lb-forward-edge (create-edge-from-constraint :start start-node :end mid-node :weight lb))
                     (lb-backward-edge (create-edge-from-constraint :start mid-node :end start-node :weight (* -1 lb)))
                     (ub-forward-edge (create-edge-from-constraint :start mid-node :end end-node :weight (- ub lb)))
                     (ub-backward-edge (create-edge-from-constraint :start end-node :end mid-node :weight 0))
                     (lower-conditional-edge (create-edge-from-constraint :start mid-node :end end-node :weight 0
                                                                          :c? t :cnode end-node :is-upper? nil))
                     (upper-conditional-edge (create-edge-from-constraint :start end-node :end mid-node :weight (- lb ub)
                                                                          :c? t :cnode end-node :is-upper? t)))

                ;; we push both unconditional and upper case edges to the cdg
                ;; since they will be considered in allMax consistency checking
                ;; lower case edges will be placed in a separate set
                (setf (gethash end-node gamma) (observation-delay constraint))

                (push lb-forward-edge cdg)
                (push lb-backward-edge cdg)
                (push ub-forward-edge cdg)
                (push ub-backward-edge cdg)
                (push upper-conditional-edge cdg)
                (push lower-conditional-edge lower-edges)))

            ;; a controllable constraint
            (progn

              ;; we need to generate only two edges
              ;; both are unconditional
              (let ((ub-edge (create-edge-from-constraint :start start-node :end end-node :weight ub))
                    (lb-edge (create-edge-from-constraint :start end-node :end start-node :weight (* -1 lb))))

                (push ub-edge cdg)
                (push lb-edge cdg))))))

    (values cdg lower-edges gamma)))

(defun get-normalized-mapping (stnu)

  "Generate the equivalent distance graph given an stnu.

   Input: An STNU.

   Output:
   stn: the STN where the temporal events are defined
   cdg: a set that contains edges in the equivalent distance graph,
        with upper case conditional edges
   lower-edges: the set of edges that are lower case conditional
   positive-map: mapping from CDG distance edges to STNU constraints
   gamma: mapping between contingent constraints and observation delay
   event-node-map: mapping from CDG nodes (i.e., events in `stn`) to
                   STNU events"

  ;; here we use simple temporal constraints with
  ;; one bound to represent edges

  (let* ((cdg nil)
         (lower-edges nil)
         (stn (make-simple-temporal-network (gensym "CDG-EQV-STN-")))
         (positive-map (make-hash-table :test 'equal))
         (gamma (make-hash-table :test 'equal))
         (event-node-map (make-hash-table :test 'equal)))
    (do-constraints (constraint stnu)

      ;; (let* ((start (temporal-constraint-start constraint))
      ;;        (end (temporal-constraint-end constraint))
      ;;        (duration (temporal-constraint-duration constraint)))

      ;;   (format t "~A-->~A:[~A,~A]  ~A~%" (event-name start) (event-name end)
      ;;           (duration-lower-bound duration) (duration-upper-bound duration) (temporal-duration-controllable? duration)))

      (let ((start-node (make-instance 'temporal-event :network stn))
            (end-node (make-instance 'temporal-event :network stn))
            (lb (lower-bound constraint))
            (ub (upper-bound constraint)))

        (if (gethash (from-event constraint) event-node-map)
            (setf start-node (gethash (from-event constraint) event-node-map))
            (setf (gethash (from-event constraint) event-node-map) start-node))

        (if (gethash (to-event constraint) event-node-map)
            (setf end-node (gethash (to-event constraint) event-node-map))
            (setf (gethash (to-event constraint) event-node-map) end-node))

        (if (not (typep constraint 'simple-temporal-constraint))

            ;; an uncontrollable constraint
            (progn

              ;; we need to generate six edges
              ;; four unconditional plus two conditional
              (let* ((mid-node (make-instance 'temporal-event :network stn))
                     (lb-forward-edge (create-edge-from-constraint :start start-node :end mid-node :weight lb))
                     (lb-backward-edge (create-edge-from-constraint :start mid-node :end start-node :weight (* -1 lb)))
                     (ub-forward-edge (create-edge-from-constraint :start mid-node :end end-node :weight (- ub lb)))
                     (ub-backward-edge (create-edge-from-constraint :start end-node :end mid-node :weight 0))
                     (lower-conditional-edge (create-edge-from-constraint :start mid-node :end end-node :weight 0
                                                                          :c? t :cnode end-node :is-upper? nil))
                     (upper-conditional-edge (create-edge-from-constraint :start end-node :end mid-node :weight (- lb ub)
                                                                          :c? t :cnode end-node :is-upper? t)))

                ;; Associate each distance edge with
                ;; 1. the positive constraint that supports it (+)
                ;; 2. the negative constraint that it 'destroys' (-)
                ;; see slides for (Yu, Fang and Williams 2014)

                (setf (gethash lb-forward-edge positive-map) `((,constraint ,:LB ,:+)))
                (setf (gethash lb-backward-edge positive-map) `((,constraint ,:LB ,:-)))

                (setf (gethash ub-forward-edge positive-map) `((,constraint ,:UB ,:+) (,constraint ,:LB ,:-)))
                (setf (gethash ub-backward-edge positive-map) nil)

                (setf (gethash lower-conditional-edge positive-map) nil)
                (setf (gethash upper-conditional-edge positive-map) `((,constraint ,:UB ,:-) (,constraint ,:LB ,:+)))

                ;; we push both unconditional and upper case edges to the cdg
                ;; since they will be considered in allMax consistency checking
                ;; lower case edges will be placed in a separate set
                (setf (gethash end-node gamma) (observation-delay constraint))

                (push lb-forward-edge cdg)
                (push lb-backward-edge cdg)
                (push ub-forward-edge cdg)
                (push ub-backward-edge cdg)
                (push upper-conditional-edge cdg)
                (push lower-conditional-edge lower-edges)))

            ;; a controllable constraint
            (progn

              ;; we need to generate only two edges
              ;; both are unconditional
              (let ((ub-edge (create-edge-from-constraint :start start-node :end end-node :weight ub))
                    (lb-edge (create-edge-from-constraint :start end-node :end start-node :weight (* -1 lb))))

                (setf (gethash ub-edge positive-map) `((,constraint ,:UB ,:+)))
                (setf (gethash lb-edge positive-map) `((,constraint ,:LB ,:-)))

                (push ub-edge cdg)
                (push lb-edge cdg))))))

    (values stn cdg lower-edges positive-map gamma event-node-map)))

(defun get-unnormalized-mapping (stnu)

  "Generate the equivalent distance graph given an stnu. This is the
   unnormalized version of `get-normalized-mapping`.

   Input: An STNU.

   Output:
   stn: the STN where the temporal events are defined
   cdg: a set that contains edges in the equivalent distance graph,
        with upper case conditional edges
   lower-edges: the set of edges that are lower case conditional
   positive-map: mapping from CDG distance edges to STNU constraints
   gamma: mapping between contingent constraints and observation delay
   event-node-map: mapping from CDG nodes (i.e., events in `stn`) to
                   STNU events"

  (let* ((cdg nil)
         (lower-edges nil)
         (stn (make-simple-temporal-network (gensym "CDG-EQV-STN-")))
         (positive-map (make-hash-table :test 'equal))
         (gamma (make-hash-table :test 'equal))
         (event-node-map (make-hash-table :test 'equal)))
    (do-constraints (constraint stnu)

      (let ((start-node (make-instance 'temporal-event :network stn))
            (end-node (make-instance 'temporal-event :network stn))
            (lb (lower-bound constraint))
            (ub (upper-bound constraint)))

        (if (gethash (from-event constraint) event-node-map)
            (setf start-node (gethash (from-event constraint) event-node-map))
            (setf (gethash (from-event constraint) event-node-map) start-node))

        (if (gethash (to-event constraint) event-node-map)
            (setf end-node (gethash (to-event constraint) event-node-map))
            (setf (gethash (to-event constraint) event-node-map) end-node))

        (if (not (typep constraint 'simple-temporal-constraint))

            ;; an uncontrollable constraint
            ;; we need to generate six edges
            ;; four unconditional plus two conditional
            (let* ((lower-conditional-edge
                    (create-edge-from-constraint :start start-node :end end-node :weight lb
                                                 :c? t :cnode end-node :is-upper? nil))
                   (upper-conditional-edge
                    (create-edge-from-constraint :start end-node :end start-node :weight (* -1 ub)
                                                 :c? t :cnode end-node :is-upper? t)))
              (setf (gethash lower-conditional-edge positive-map) `((,constraint ,:LB ,:+)))
              (setf (gethash upper-conditional-edge positive-map) `((,constraint ,:UB ,:-)))
              (setf (gethash end-node gamma) (observation-delay constraint))
              (push upper-conditional-edge cdg)
              (push lower-conditional-edge lower-edges))

            ;; a controllable constraint
            ;; we need to generate only two edges
            ;; both are unconditional
            (let ((ub-edge (create-edge-from-constraint :start start-node :end end-node :weight ub))
                  (lb-edge (create-edge-from-constraint :start end-node :end start-node :weight (* -1 lb))))
              (setf (gethash ub-edge positive-map) `((,constraint ,:UB ,:+)))
              (setf (gethash lb-edge positive-map) `((,constraint ,:LB ,:-)))
              (push ub-edge cdg)
              (push lb-edge cdg)))))

    (values stn cdg lower-edges positive-map gamma event-node-map)))

(defun reduce-edge (edge1 edge2)

  (assert (eq (d-edge-to-node edge1) (d-edge-from-node edge2)))
  (let* ((new-start (d-edge-from-node edge1))
         (new-end (d-edge-to-node edge2))
         (new-value (+ (d-edge-weight edge1) (d-edge-weight edge2)))
         (new-type nil)
         (new-maybe-letter nil))
    (cond
      ;; UPPER CASE REDUCTION
      ((and (eq (d-edge-is-upper-case? edge1) :NO-CASE) (eq (d-edge-is-upper-case? edge2) :UPPER-CASE))
       (progn
         (setf new-type :UPPER-CASE)
         (setf new-maybe-letter (d-edge-conditioned-node edge2))))
      ;; LOWER CASE REDUCTION
      ((and (eq (d-edge-is-upper-case? edge1) :LOWER-CASE)
            (eq (d-edge-is-upper-case? edge2) :NO-CASE)
            (< (d-edge-weight edge2) 0))
       (setf new-type :NO-CASE))
      ;; CROSS CASE REDUCTION
      ((and (eq (d-edge-is-upper-case? edge1) :LOWER-CASE)
            (eq (d-edge-is-upper-case? edge2) :UPPER-CASE)
            (< (d-edge-weight edge2) 0)
            (not (eq (d-edge-conditioned-node edge1) (d-edge-conditioned-node edge2))))
       (progn
         (setf new-type :UPPER-CASE)
         (setf new-maybe-letter (d-edge-conditioned-node edge2))))
      ;; NO CASE REDUCTION
      ((and (eq (d-edge-is-upper-case? edge1) :NO-CASE) (eq (d-edge-is-upper-case? edge2) :NO-CASE))
       (setf new-type :NO-CASE)))

    ;; TRY APPLYING LABEL REMOVAL IF POSSIBLE
    (when (and (eq new-type :UPPER-CASE) (>= new-value 0))
      (setf new-type :NO-CASE)
      (setf new-maybe-letter nil))

    (if (eq new-type nil)
        nil
        (create-edge new-start
                     new-end
                     new-value
                     :c? new-type
                     :cnode new-maybe-letter))))


(defun create-edge-from-constraint (&key (start nil)
                                      (end nil)
                                      (weight nil)
                                      (c? nil)
                                      (cnode nil)
                                      (is-upper? nil))

  (when (not c?)
    ;; not conditional
    (return-from create-edge-from-constraint (create-edge start end weight)))

  (create-edge start end weight :c? c? :cnode cnode :upper? is-upper?))


(defun print-edge-set (path)

  "Print all edges in the given path"

  (dolist (edge path)
    (print-edge edge)
    (format t "~%")))


(defun print-edge (edge)

  "Print the distance edge"

  (if (not edge)
      (format t "nil")
      (progn
        (format t "~A->~A(~A) " (name (d-edge-from-node edge)) (name (d-edge-to-node edge)) (d-edge-weight edge))
        (when (d-edge-conditional? edge)
          (if (d-edge-is-upper-case? edge)
              (format t "U:")
              (format t "L:"))
          (format t "~A" (name (d-edge-conditioned-node edge)))))))
