(in-package :temporal-controllability)

(defconstant +inf+ float-features:double-float-positive-infinity)
(defconstant +-inf+ float-features:double-float-negative-infinity)
