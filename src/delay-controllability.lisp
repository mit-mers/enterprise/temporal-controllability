(in-package :temporal-controllability)

(defun delay-controllable? (stnu)
  "Implements a delay controllability checker. The first return value is whether
   the system is delay controllable. The second is the value of the conflict (if
   one exists)."
  (multiple-value-bind (cdg lower-edges gamma)
      (get-simplified-mapping stnu)
    (delay-controllability-internal (graph-as-reverse-adjacency-list (append cdg lower-edges)) gamma)))

(defun delay-controllability-internal (graph gamma)
  "Internal call that should be used by iterative methods for testing..."
  (let* ((global-predecessors (make-hash-table :test 'equal))
         (novel-edges (make-hash-table :test 'equal))
         (lower-combos (make-hash-table :test 'equal))
         (negative-nodes (nodes-with-negative-edges graph)))
    (loop for start-node being the hash-keys in negative-nodes using (hash-value value) do
         (when value
           (multiple-value-bind (result edges)
               (delay-dijkstra graph
                               gamma
                               global-predecessors
                               novel-edges
                               lower-combos
                               start-node
                               (list start-node)
                               negative-nodes)
             (when (not result)
               (return-from delay-controllability-internal (values nil
                                                                   edges
                                                                   (graph-without-novel graph novel-edges)
                                                                   gamma
                                                                   graph))))))
    (values t
            nil
            (graph-without-novel graph novel-edges)
            gamma
            graph)))

(defun graph-without-novel (graph novel-edges)
  (let ((result (make-hash-table :test 'equal)))
    (loop for node being the hash-keys in graph using (hash-value edges) do
         (setf (gethash node result) (remove-if #'(lambda (value) (gethash value novel-edges)) edges)))
    result))

(defun delay-dijkstra (graph gamma global-predecessors novel-edges lower-combos start-node maintained-stack negative-nodes)
  (cond
    ((member start-node (cdr maintained-stack)) (values nil nil start-node))
    (t (let ((min-distances (make-hash-table :test 'equal))
             (predecessors (make-hash-table :test 'equal))
             (all-nodes (loop for node being the hash-keys of graph collect node))
             (queue (make-queue :priority-queue :compare #'queue-struct<)))
         (dolist (node all-nodes)
           (setf (gethash node min-distances) +inf+))
         (setf (gethash start-node min-distances) 0)
         (dolist (edge (gethash start-node graph))
           (let ((from-node (d-edge-from-node edge))
                 (label (d-edge-conditioned-node edge))
                 (weight (d-edge-weight edge)))
             (when (and (< weight 0)
                        (or (null (gethash from-node min-distances))
                            (< weight (gethash from-node min-distances))))
               (setf (gethash from-node min-distances) weight)
               (setf (gethash from-node predecessors) edge)
               (qpush queue (make-queue-struct :weight weight
                                               :node from-node
                                               :label label)))))
         (loop while (> (qsize queue) 0) do
              (let* ((qs (qpop queue))
                     (current-node (queue-struct-node qs))
                     (label (queue-struct-label qs))
                     (node-distance (gethash current-node min-distances)))
                (if (>= node-distance 0)
                    (let ((edge-list (gethash start-node graph))
                          (new-edge (create-edge current-node
                                                 start-node
                                                 node-distance)))
                      (setf (gethash start-node graph) (cons new-edge edge-list))
                      (setf (gethash new-edge novel-edges) t))
                    (progn
                      (when (gethash current-node negative-nodes)
                        (multiple-value-bind (result edges terminating-node)
                            (delay-dijkstra graph
                                            gamma
                                            global-predecessors
                                            novel-edges
                                            lower-combos
                                            current-node
                                            (cons current-node maintained-stack)
                                            negative-nodes)
                          (when (not result)
                            (when (not (null terminating-node))
                              (let ((new-edges (list))
                                  (operating-node current-node))
                                (loop while (not (null operating-node)) do
                                     (let ((preceding-edge (gethash operating-node predecessors)))
                                       (cond
                                         ((gethash preceding-edge novel-edges) (setf new-edges (append (get-edges-composing-novel-set preceding-edge
                                                                                                                                      novel-edges
                                                                                                                                      global-predecessors
                                                                                                                                      lower-combos)
                                                                                                       new-edges)))
                                         ((gethash preceding-edge lower-combos) (setf new-edges (append (gethash preceding-edge lower-combos)
                                                                                                        new-edges)))
                                         (t (setf new-edges (cons preceding-edge new-edges))))
                                       (setf operating-node (d-edge-to-node preceding-edge))
                                       (if (equal operating-node start-node)
                                           (setf operating-node nil))))
                                (setf edges (append new-edges edges)))
                              (if (equal terminating-node start-node)
                                  (setf terminating-node nil)))
                            (return-from delay-dijkstra (values nil edges terminating-node)))))
                      (dolist (edge (gethash current-node graph))
                        (if (and (>= (d-edge-weight edge) 0)
                                 (or (null label)
                                     (null (d-edge-conditioned-node edge))
                                     (d-edge-is-upper-case? edge)
                                     (not (equal label
                                                 (d-edge-conditioned-node edge)))))
                            (let* ((new-weight (+ node-distance (d-edge-weight edge)))
                                   (from-node (d-edge-from-node edge)))
                              (when (< new-weight (gethash from-node min-distances))
                                (setf (gethash from-node min-distances) new-weight)
                                (setf (gethash from-node predecessors) edge)
                                (qpush queue (make-queue-struct :weight new-weight
                                                                :node from-node
                                                                :label label)))
                                        ; do the lower-case extension
                              (let ((incoming-edges (gethash from-node graph)))
                                (dolist (lower-edge incoming-edges)
                                  (when (and (not (null (d-edge-conditioned-node lower-edge)))
                                             (not (d-edge-is-upper-case? lower-edge))
                                             (< (d-edge-weight edge) (gethash (d-edge-conditioned-node lower-edge) gamma))
                                             (> (gethash (d-edge-from-node lower-edge) min-distances) new-weight)) ; all lower-case edges have 0 weight
                                    (let* ((lower-from (d-edge-from-node lower-edge))
                                           (combo-edge (create-edge lower-from
                                                                    current-node
                                                                    (d-edge-weight edge))))
                                      (if (gethash edge novel-edges)
                                          (setf (gethash combo-edge lower-combos) (append (get-edges-composing-novel-set edge
                                                                                                                         novel-edges
                                                                                                                         global-predecessors
                                                                                                                         lower-combos)
                                                                                          (list lower-edge)))
                                          (setf (gethash combo-edge lower-combos) (list edge lower-edge)))
                                      (setf (gethash lower-from min-distances) new-weight)
                                      (setf (gethash lower-from predecessors) combo-edge)
                                      (qpush queue (make-queue-struct :weight new-weight
                                                                      :node lower-from
                                                                      :label label)))))))))))))
         (setf (gethash start-node negative-nodes) nil)
         (setf (gethash start-node global-predecessors) predecessors)
         (values t nil nil)))))
