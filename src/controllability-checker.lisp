;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defclass dynamic-controllability-checker ()
  ((stnu
    :initarg :temporal-network
    :reader stnu
    :documentation "The temporal network object for which dynamic controllability
is being checked.")))

(defmethod temporal-network-consistent? ((dc-checker dynamic-controllability-checker) &rest args)
  (declare (ignore args))
  (dynamically-controllable? (stnu dc-checker) :algorithm :n3))

(defclass strong-controllability-checker ()
  ((stnu
    :initarg :temporal-network
    :reader stnu
    :documentation "The temporal network object for which dynamic controllability
is being checked.")))

(defmethod temporal-network-consistent? ((sc-checker strong-controllability-checker) &rest args)
  (declare (ignore args))
  (strongly-controllable? (stnu sc-checker)))

(defclass delay-controllability-checker ()
  ((stnu
    :initarg :temporal-network
    :reader stnu
    :documentation "The temporal network object for which delay controllability
is being checked.")))

(defmethod temporal-network-consistent? ((delay-checker delay-controllability-checker) &rest args)
  (declare (ignore args))
  (delay-controllable? (stnu delay-checker)))
