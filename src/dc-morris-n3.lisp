(in-package :temporal-controllability)

;; TODO: The current implementation only works on the `:normalized`
;; option. A parallel implementation could and should be made for the
;; `:unnormalized` option.
(defparameter *cdg-type* :normalized)

(defun dynamically-controllable?-n3 (stnu)
  "Implements the DC checking algorithm from (Morris 2014).

   Input: An STNU.

   Output: A boolean representing whether the STNU is controllable. It also
   outputs a conflict if the network is uncontrollable."

  (multiple-value-bind (stn cdg lower-edges positive-map gamma event-node-map)
      (ecase *cdg-type*
        (:normalized (get-normalized-mapping stnu))
        (:unnormalized (get-unnormalized-mapping stnu)))

    (declare (ignore stn))
    (declare (ignore gamma))
    (let* ((graph (graph-as-reverse-adjacency-list (append cdg lower-edges)))
           (global-predecessors (make-hash-table :test 'equal))
           (completed-calls-dag (make-hash-table :test 'equal))
           (novel-edges (make-hash-table :test 'equal))
           (negative-nodes (nodes-with-negative-edges graph)))
      (run-main-dc-n3 graph
                      global-predecessors
                      completed-calls-dag
                      novel-edges
                      negative-nodes
                      positive-map
                      event-node-map))))

(defun run-main-dc-n3 (graph global-predecessors dag novel-edges
                       negative-nodes positive-map event-node-map)
  (let ((ids (make-incremental-data-structures)))
    (loop for start-node being the hash-keys in negative-nodes using (hash-value value) do
         (when value
           (multiple-value-bind (result edges)
               (rebooting-dijkstra graph
                                   global-predecessors
                                   dag
                                   novel-edges
                                   start-node
                                   (list start-node)
                                   negative-nodes)
             (when (not result)
               (setf (incremental-data-structures-shortest-path-predecessors ids) global-predecessors)
               (setf (incremental-data-structures-completed-calls-dag ids) dag)
               (setf (incremental-data-structures-negative-nodes ids) negative-nodes)
               (setf (incremental-data-structures-novel-edges ids) novel-edges)
               (setf (incremental-data-structures-graph ids) graph)
               (setf (incremental-data-structures-cdg-conflicts ids) (cdg-conflicts edges positive-map))
               (setf (incremental-data-structures-positive-map ids) positive-map)
               (setf (incremental-data-structures-event-node-map ids) event-node-map)
               (return-from run-main-dc-n3
                            (values nil
                                    (conflict-from-edges edges positive-map event-node-map)
                                    ids))))))
    (values t nil nil)))

(defun cdg-conflicts (edges positive-map)
  (let ((main-cycle (list))
        (main-weight 0)
        (lower-reductions (cdg-lower-reduction-conflicts (reverse edges))))
    (dolist (edge edges)
      (when (and (or (not (d-edge-conditional? edge))
                     (d-edge-is-upper-case? edge))
                 (not (null (gethash edge positive-map))))
        (setf main-cycle (cons edge main-cycle))
        (incf main-weight (d-edge-weight edge))))
    (cons (list main-cycle main-weight) lower-reductions)))

(defun cdg-lower-reduction-conflicts (edges)
  (if (not (null edges))
      (let ((edge (car edges)))
        (if (not (d-edge-is-lower-case? edge))
            (cdg-lower-reduction-conflicts (cdr edges))
            (let ((cycle (list))
                  (weight 0)
                  (rest (cdr edges)))
              (loop while (and (>= weight 0)
                               (not (null rest))) do
                    (let ((edge (car rest)))
                      (unless (d-edge-is-lower-case? edge)
                        (push edge cycle))
                      (incf weight (d-edge-weight edge))
                      (setf rest (cdr rest))))
              (cons (list cycle weight) (cdg-lower-reduction-conflicts (cdr edges))))))
      nil))

(defun conflict-from-edges (edges positive-map event-node-map)
  (let ((conflict (create-temporal-conflict))
        (ncycle (create-negative-cycle))
        (weight 0))
    (dolist (edge edges)
      (dolist (support (gethash edge positive-map))
        (add-constraint-to-cycle ncycle support))
      (incf weight (d-edge-weight edge)))
    (setf (negative-cycle-value ncycle) weight)
    (add-cycle-to-conflict conflict ncycle)
    (add-supports-for-lower-case-reductions (reverse edges) positive-map
                                            conflict event-node-map)
    conflict))

(defun add-supports-for-lower-case-reductions (edges positive-map
                                               conflict event-node-map)
  (if (not (null edges))
      (let ((edge (car edges)))
        (if (not (d-edge-is-lower-case? edge))
            (add-supports-for-lower-case-reductions (cdr edges) positive-map
                                                    conflict event-node-map)
            (let ((extension-subpath (create-negative-cycle))
                  (rest (cdr edges))
                  (weight 0)
                  (last-support))
              (loop while (and (>= weight 0)
                               (not (null rest)))
                 do (loop for support in (gethash (car rest) positive-map)
                       do (add-constraint-to-cycle extension-subpath support)
                       finally (setf last-support support))
                    (incf weight (d-edge-weight (car rest)))
                    (setf rest (cdr rest))
                 finally
                   (destructuring-bind (constraint bound direction) last-support
                     (when (and (typep constraint 'simple-contingent-temporal-constraint)
                                (eql bound :lb)
                                (eql direction :+))
                       (remove-constraint-from-cycle extension-subpath last-support)
                       (decf weight (lower-bound constraint)))))
              (when (< weight 0)
                (setf (negative-cycle-value extension-subpath) weight)
                (add-cycle-to-conflict conflict extension-subpath))
              (add-supports-for-lower-case-reductions (cdr edges) positive-map
                                                      conflict event-node-map))))))

(defstruct queue-struct
  (weight 0)
  (node nil)
  (label nil))

(defun queue-struct< (q1 q2)
  (< (queue-struct-weight q1)
     (queue-struct-weight q2)))

(defun rebooting-dijkstra (graph global-predecessors completed-calls-dag novel-edges start-node maintained-stack negative-nodes)
  "Performs the Rebooting Dijkstra algorithm to detect dynamic controllability"
  (cond
    ((member start-node (cdr maintained-stack)) (values nil nil start-node))
    (t (let ((min-distances (make-hash-table :test 'equal))
             (predecessors (make-hash-table :test 'equal))
             (dag-links (make-hash-table :test 'equal))
             (all-nodes (loop for node being the hash-keys of graph collect node))
             (queue (make-queue :priority-queue :compare #'queue-struct<)))
         (dolist (node all-nodes)
           (setf (gethash node min-distances) +inf+))
         (setf (gethash start-node min-distances) 0)
         (dolist (edge (gethash start-node graph))
           (let ((from-node (d-edge-from-node edge))
                 (label (d-edge-conditioned-node edge))
                 (weight (d-edge-weight edge)))
             (when (and (< weight 0)
                        (or (null (gethash from-node min-distances))
                            (< weight (gethash from-node min-distances))))
               (setf (gethash from-node min-distances) weight)
               (setf (gethash from-node predecessors) edge)
               (qpush queue (make-queue-struct :weight weight
                                               :node from-node
                                               :label label)))))
         (loop while (> (qsize queue) 0) do
              (let* ((qs (qpop queue))
                     (current-node (queue-struct-node qs))
                     (label (queue-struct-label qs))
                     (node-distance (gethash current-node min-distances)))
                (if (>= node-distance 0)
                    (let ((edge-list (gethash start-node graph))
                          (new-edge (create-edge current-node
                                                 start-node
                                                 node-distance)))
                      (setf (gethash start-node graph) (cons new-edge edge-list))
                      (setf (gethash new-edge novel-edges) t))
                    (progn
                      (when (gethash current-node negative-nodes)
                        (multiple-value-bind (result edges terminating-node)
                            (rebooting-dijkstra graph
                                                global-predecessors
                                                completed-calls-dag
                                                novel-edges
                                                current-node
                                                (cons current-node maintained-stack)
                                                negative-nodes)
                          (when (not result)
                            (when (not (null terminating-node))
                              (let ((new-edges (list))
                                    (operating-node current-node))
                                (loop while (not (null operating-node)) do
                                     (let ((preceding-edge (gethash operating-node predecessors)))
                                       (if (gethash preceding-edge novel-edges)
                                           (setf new-edges
                                                 (append (get-edges-composing-novel-set
                                                          preceding-edge novel-edges
                                                          global-predecessors (make-hash-table :test 'equal))
                                                         new-edges))
                                           (setf new-edges (cons preceding-edge new-edges)))
                                       (setf operating-node (d-edge-to-node preceding-edge))
                                       (if (equal operating-node start-node)
                                           (setf operating-node nil))))
                                (setf edges (append new-edges edges)))
                              (if (equal terminating-node start-node)
                                  (setf terminating-node nil)))
                            (return-from rebooting-dijkstra (values nil edges terminating-node)))))
                      (when (second (multiple-value-list (gethash current-node negative-nodes)))
                        (setf (gethash current-node dag-links) t))
                      (dolist (edge (gethash current-node graph))
                        (if (and (>= (d-edge-weight edge) 0)
                                 (or (null label)
                                     (null (d-edge-conditioned-node edge))
                                     (d-edge-is-upper-case? edge)
                                     (not (equal label
                                                 (d-edge-conditioned-node edge)))))
                            (let* ((new-weight (+ node-distance (d-edge-weight edge)))
                                   (from-node (d-edge-from-node edge)))
                              (when (< new-weight (gethash from-node min-distances))
                                (setf (gethash from-node min-distances) new-weight)
                                (setf (gethash from-node predecessors) edge)
                                (qpush queue (make-queue-struct :weight new-weight
                                                                :node from-node
                                                                :label label))))))))))
         (setf (gethash start-node negative-nodes) nil)
         (setf (gethash start-node global-predecessors) predecessors)
         (setf (gethash start-node completed-calls-dag) dag-links)
         (values t nil nil)))))

(defun label-removal-applies? (label weight reverse-adjacency-graph)
  ;; Note: `label` is a node, i.e., event.
  (let* ((incoming-edges (gethash label reverse-adjacency-graph))
         (lowercase-edge (find-if (lambda (edge)
                                    (with-slots (conditional? conditioned-node is-upper-case?)
                                        edge
                                      (and conditional?
                                           (not is-upper-case?)
                                           (eql conditioned-node label))))
                                  incoming-edges))
         (lower-bound (d-edge-weight lowercase-edge)))
    (>= weight (* -1 lower-bound))))

(defun get-edges-composing-novel-set (edge novel-edges global-predecessors lower-combos)
  (let* ((end-node (d-edge-to-node edge))
         (operating-node (d-edge-from-node edge))
         (predecessors (gethash end-node global-predecessors))
         (output (list)))
    (loop while (not (equal end-node operating-node)) do
         (let ((preceding-edge (gethash operating-node predecessors)))
           (if (null preceding-edge)
               (return-from get-edges-composing-novel-set output))
           (if (gethash preceding-edge lower-combos)
               (setf output (append (gethash preceding-edge lower-combos) output))
               (if (gethash preceding-edge novel-edges)
                   (setf output (append (get-edges-composing-novel-set preceding-edge novel-edges global-predecessors lower-combos)
                                        output))
                   (setf output (cons preceding-edge output))))
           (setf operating-node (d-edge-to-node preceding-edge))))
    output))

(defun graph-as-reverse-adjacency-list (edges)
  (let ((node-to-edges (make-hash-table :test 'equal)))
    (dolist (edge edges)
      (let* ((end-node (d-edge-to-node edge))
             (edge-list (gethash end-node node-to-edges)))
        (setf (gethash end-node node-to-edges) (cons edge edge-list))))
    node-to-edges))

(defun nodes-with-negative-edges (graph)
  (let ((negative-nodes (make-hash-table :test 'equal)))
    (loop for start-node being the hash-keys in graph using (hash-value edges) do
         (dolist (edge edges)
           (if (< (d-edge-weight edge) 0)
               (setf (gethash start-node negative-nodes) t))))
    negative-nodes))
