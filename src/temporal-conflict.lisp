;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defclass temporal-conflict ()

  ((negative-cycles
    :initform nil
    :initarg :negative-cycles
    :accessor temporal-conflict-cycles
    :documentation "a disjunctive set of negative cycles"))


  (:documentation "a temporal conflict object that explains the cause of infeasibility"))


(defun create-temporal-conflict (&key (cycles nil))

  "Create a temporal conflict object
   using the given negative cycles

   Note that a temporal conflict may have more than one cycles: a
   conflict can be resolved if any of these cycles are eliminated."

  (make-instance 'temporal-conflict
                 :negative-cycles cycles))


(defun add-cycle-to-conflict (conflict new-cycle)

  "Add a new cycle to the conflict"

  (push new-cycle (temporal-conflict-cycles conflict)))
