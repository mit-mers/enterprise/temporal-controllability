;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defun strongly-controllable? (stnu)
  "Check whether an STNU is strongly controllable, returning a
   negative cycle if not.

   Input: An STNU.

   Output: Whether the STNU is strongly controllable.
   Output: A negative cycle in terms of the STNU's edges, or `NIL` if
           one does not exist. Each edge in the cycle is of the form
           `(CONSTRAINT :LB/:UB)`."

  ;; Reformulate via reductions into an STN, mapping the STN's edges
  ;; back to lists of the STNU's edges.
  (multiple-value-bind (stn sources)
      (reformulate-sc stnu)
    ;; Check consistency on the STN. Return if so.
    (multiple-value-bind (consistent stn-neg-cycle)
        (temporal-network-consistent?
         (make-itc stn :itc-method :negative-cycle-detection))
      (when consistent
        (return-from strongly-controllable? (values t nil)))
      ;; If not consistent, map the STN's negative cycle into one on
      ;; the STNU's edges, and return that.
      (let* ((mapped-stn-edges (mapcar #'(lambda (stn-edge)
                                           (gethash stn-edge sources))
                                       stn-neg-cycle))
             (stnu-neg-cycle (reduce #'append mapped-stn-edges)))
        (values nil stnu-neg-cycle)))))

(defun reformulate-sc (stnu)
  "Perform strong controllability reductions on an STNU to yield a
   reformulated STN. Keep track of which STNU edges contributed to
   each edge in the STN.

   Input: An STNU.

   Output: The reformulated STN.
   Output: A hash table mapping each STN edge to a list of STNU
           edges which contributed to its value. An edge is of the
           form `(CONSTRAINT :LB/:UB)`."

  (let ((stn (make-simple-temporal-network :reformulated-stn))
        (stnu-copy (make-instance 'temporal-network :features (features stnu)))
        (stnu-constraints)
        (sources (make-hash-table :test 'equal)))

    ;; Initialize an STN with only the controllable events.
    (do-events (event stnu)
      (unless (contingent-constraint event)
        (make-instance 'temporal-event
                       :id (id event)
                       :name (name event)
                       :network stn)))

    ;; Make a copy of the STNU.
    (do-events (event stnu)
      (make-instance 'temporal-event
                     :id (id event)
                     :name (name event)
                     :network stnu-copy))
    (do-constraints (constraint stnu)
      (make-instance (type-of constraint)
                     :id (id constraint)
                     :name (name constraint)
                     :from-event (find-event stnu-copy (id (from-event constraint)))
                     :to-event (find-event stnu-copy (id (to-event constraint)))
                     :lower-bound (lower-bound constraint)
                     :upper-bound (upper-bound constraint)
                     :network stnu-copy))

    ;; Find the set of all controllable constraints in the STNU.
    (do-constraints (constraint stnu-copy)
      (when (temporal-duration-controllable? constraint)
        (push constraint stnu-constraints)))

    ;; Reformulate all the STNU's controllable constraints to produce
    ;; the STN. For each controllable edge, push it back along
    ;; uncontrollable durations until it spans two controllable
    ;; events.
    (loop for constraint in stnu-constraints do
       ;; Initialize the sources table.
         (setf (gethash `(,constraint ,:lb) sources) `((,constraint ,:lb)))
         (setf (gethash `(,constraint ,:ub) sources) `((,constraint ,:ub)))
       ;; Perform the reductions and insert the resulting
       ;; constraint in the STN.
         (loop until (controllable-endpoints? constraint) do
              (setf constraint (dominate constraint sources)))
         (let ((stn-constraint
                (make-instance 'simple-temporal-constraint
                               :lower-bound (lower-bound constraint)
                               :upper-bound (upper-bound constraint)
                               :from-event (find-event stn (id (from-event constraint)))
                               :to-event (find-event stn (id (to-event constraint)))
                               :network stn)))
           (setf (gethash `(,stn-constraint ,:lb) sources)
                 (gethash `(,constraint ,:lb) sources))
           (remhash `(,constraint ,:lb) sources)
           (setf (gethash `(,stn-constraint ,:ub) sources)
                 (gethash `(,constraint ,:ub) sources))
           (remhash `(,constraint ,:ub) sources)))

    ;; Replace the (constraint :lb/:ub) pairs in sources with
    ;; constraints from the original STNU and not the copied,
    ;; scratchpad STNU.
    (maphash (lambda (stn-edge stnu-copy-edges)
               (setf (gethash stn-edge sources)
                     (mapcar (lambda (stnu-copy-edge)
                               (destructuring-bind (constraint direction)
                                   stnu-copy-edge
                                 `(,(find-temporal-constraint stnu (id constraint))
                                   ,direction)))
                             stnu-copy-edges)))
             sources)

    (values stn sources)))

(defun controllable-endpoints? (constraint)
  (let ((start (from-event constraint))
        (end (to-event constraint)))
    (dolist (event `(,start ,end))
      (dolist (incoming (incoming-constraints event))
        (unless (temporal-duration-controllable? incoming)
          (return-from controllable-endpoints?
                       (values nil incoming)))))
    (values t nil)))

(defun dominate (constraint sources)
  ;; Let S and E be the current constraint's start and end events.
  ;; Let L and U be the current constraint's LB and UB.
  ;; Let s and e be the uncontrollable duration's start and end events.
  ;; Let l and u be the uncontrollable duration's LB and UB.

  ;; Case S == e:
  ;; new constraint s --> E with [L+u, U+l]
  ;; sources[new L] = sources[old L], u
  ;; sources[new U] = sources[old U], l

  ;; Case E == e:
  ;; new constraint S --> s with [L-l, U-u]
  ;; sources[new L] = sources[old L], l
  ;; sources[new U] = sources[old U], u

  (multiple-value-bind (done uncontrollable-constraint)
      (controllable-endpoints? constraint)
    (when done
      (return-from dominate constraint))
    (let* ((cc constraint)
           (s-cc (from-event cc))
           (e-cc (to-event cc))
           (l-cc (lower-bound cc))
           (u-cc (upper-bound cc))
           (uc uncontrollable-constraint)
           (s-uc (from-event uc))
           (e-uc (to-event uc))
           (l-uc (lower-bound uc))
           (u-uc (upper-bound uc))
           nc)
      (cond ((eql s-cc e-uc)
             (setf nc (make-instance 'simple-temporal-constraint
                                     :from-event s-uc
                                     :to-event e-cc
                                     :lower-bound (+ l-cc u-uc)
                                     :upper-bound (+ u-cc l-uc)
                                     :network (tn::network constraint)))
             (setf (gethash `(,nc ,:lb) sources)
                   (append (gethash `(,cc ,:lb) sources) `((,uc ,:ub))))
             (remhash `(,cc ,:lb) sources)
             (setf (gethash `(,nc ,:ub) sources)
                   (append (gethash `(,cc ,:ub) sources) `((,uc ,:lb))))
             (remhash `(,cc ,:ub) sources))

            ((eql e-cc e-uc)
             (setf nc (make-instance 'simple-temporal-constraint
                                     :from-event s-cc
                                     :to-event s-uc
                                     :lower-bound (- l-cc l-uc)
                                     :upper-bound (- u-cc u-uc)
                                     :network (tn::network constraint)))
             (setf (gethash `(,nc ,:lb) sources)
                   (append (gethash `(,cc ,:lb) sources) `((,uc ,:lb))))
             (remhash `(,cc ,:lb) sources)
             (setf (gethash `(,nc ,:ub) sources)
                   (append (gethash `(,cc ,:ub) sources) `((,uc ,:ub))))
             (remhash `(,cc ,:ub) sources))

            (t (setf nc cc)))
      (remove-temporal-constraint! (tn::network constraint) constraint)
      nc)))

(defun temporal-duration-controllable? (duration)
  (and (typep duration 'simple-temporal-constraint)
       (not (typep duration 'simple-contingent-temporal-constraint))))
