;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-controllability)

(defclass negative-cycle ()

  ((constraints
    :initform nil
    :initarg :constraints
    :accessor negative-cycle-constraints
    :documentation "constraints in this negative cycle, could be controllable or uncontrollable")

   (constraints-ratio-map
    :initform (make-hash-table :test 'equal)
    :initarg :constraints-ratio
    :accessor negative-cycle-constraints-ratio-map
    :documentation "a hash table that maps the constraints to a positive or negative integer, representing
how many times this constraint has been counted in the negative cycle.")

   (n-value
    :initform nil
    :initarg :n-value
    :accessor negative-cycle-value
    :documentation "how negative the cycle is"))


  (:documentation "a set of constraints that are inconsistent. The negative cycle object stores a linear expression
defined over the lower and upper bounds of these constraints. The cycle is eliminated if the value of this expression
becomes positive."))

(defmethod print-object ((nc negative-cycle) stream)
  (format stream "<NEG-CYCLE~%")
  (loop :for c :in (negative-cycle-constraints nc)
        :for count = (gethash c (negative-cycle-constraints-ratio-map nc))
     :do (format stream "x~a ~a~%" count c))
  (format stream ">~%"))

(defun create-negative-cycle (&key (constraints nil)
                                (n-value nil))

  "Create a negative cycle"

  (make-instance 'negative-cycle
                 :constraints constraints
                 :n-value n-value))

(defun add-constraint-to-cycle (n-cycle constraint)

  "Add a constraint to the negative cycle"

  (let ((constraint-count (gethash constraint (negative-cycle-constraints-ratio-map n-cycle))))

    (if constraint-count

        ;; If we have recorded the constraint before.
        ;; do not push it to the list
        ;; instead update its count

        (progn
          (setf (gethash constraint (negative-cycle-constraints-ratio-map n-cycle)) (+ 1 constraint-count)))

        ;; Otherwise
        ;; Push it to the constraint list
        ;; and add it to the hashtable with count 1

        (progn
          (push constraint (negative-cycle-constraints n-cycle))
          (setf (gethash constraint (negative-cycle-constraints-ratio-map n-cycle)) 1)))))

(defun remove-constraint-from-cycle (n-cycle constraint)

  "Undo the effects of `add-constraint-to-cycle` for a single constraint"

  (let ((constraint-count (gethash constraint (negative-cycle-constraints-ratio-map n-cycle))))
    (assert constraint-count)

    (if (> constraint-count 1)

        ;; If the constraint is repeated,
        ;; do not remove it from the list
        ;; instead decrement its count

        (progn
          (setf (gethash constraint (negative-cycle-constraints-ratio-map n-cycle)) (- 1 constraint-count)))

        ;; Otherwise
        ;; Remove it from the constraint list
        ;; and remove it from the hashtable

        (progn
          (setf (negative-cycle-constraints n-cycle)
                (delete constraint (negative-cycle-constraints n-cycle)))
          (remhash constraint (negative-cycle-constraints-ratio-map n-cycle))))))
